import pandas as pd
from common_functions.athena import QueryAthena

def query_handler(query, database, filter_clause, filter_variable, filter_required=True):
    print('query_handler get_loan_list line 5')
    if filter_required:
        filter_query = f"\nWHERE {filter_clause}='{filter_variable}'"
        query = query + filter_query

    athena_handler = QueryAthena(query=query, database=database)
    df = athena_handler.run_query()
    #print(athena_handler.get_s3_file_name())
    #print(query)
    return df


def get_loan_list(body):
    p_business_type = body.get('business_type')
    
    query = f""" SELECT apps.lead_code as lead_code
        FROM los_afx_db.sme_eeg_applicants AS apps
        LEFT JOIN los_afx_db.sme_eeg_tat_data AS tat ON apps.lead_code = tat.lead_code
        WHERE (apps.lead_code LIKE 'BL%' OR apps.lead_code LIKE 'LAP%')
        AND apps.Industry NOT IN ('Education')
        AND tat.login_checklist_final_stage IS NOT NULL
        AND tat.start_date > CAST('2020-11-01' AS Date)
        GROUP BY 1"""
    if(p_business_type == 'EEG'):
        query = f""" SELECT apps.lead_code as lead_code
        FROM los_afx_db.sme_eeg_applicants AS apps
        LEFT JOIN los_afx_db.sme_eeg_tat_data AS tat ON apps.lead_code = tat.lead_code
        WHERE (apps.lead_code LIKE 'EEG-BL%' OR apps.lead_code LIKE 'EEG-LAP%')
        AND apps.Industry NOT IN ('Education')
        AND tat.login_checklist_final_stage IS NOT NULL
        AND tat.start_date > CAST('2020-11-01' AS Date)
        GROUP BY 1"""
    
    df = query_handler(query=query, database='los_afx_db', filter_clause='', filter_variable='', filter_required=False)
    
    
    print(f"length of df: {len(df)}")

    if(len(df) > 0):
        response = {
            "status" : "success",
            "error" : False,
            "data" : list(df['lead_code'].unique())
        }
    else:
        response = {
            "status" : "fail",
            "error" : True,
            "data" : []
        }
        
    return response