import sub_reports.data_config as data_config


def case_details_formulas(sheet):
    
    mapping = data_config.case_details_mapping

    mv_as_per_customer = mapping.get('mv_as_per_customer')
    res_row = mapping.get('res_row')
    sr = mapping.get('sr')
    er = mapping.get('er')
    
    
    sheet[f'{mv_as_per_customer}{res_row}'] = f'=SUM({mv_as_per_customer}{sr}:{mv_as_per_customer}{er})'
    
    return sheet
    
def fs_formulas(sheet):
    
    ##1. Balance Sheet
    col_mapping = data_config.fs_sheet_col_mapping
    row_mapping = data_config.fs_sheet_row_mapping
    
    fy2019_col = col_mapping.get('Y1')
    fy2018_col = col_mapping.get('Y2')
    fy2017_col = col_mapping.get('Y3')
    
    
    #A. Equity & Liabilities
    equity_total = row_mapping.get('equity_total')
    
    start_row = data_config.fs_equity_mapping.get('start_row')
    end_row = data_config.fs_equity_mapping.get('end_row')
    
    
    sheet[f'{fy2019_col}{equity_total}'] = f'=SUM({fy2019_col}{start_row}:{fy2019_col}{end_row})'
    sheet[f'{fy2018_col}{equity_total}'] = f'=SUM({fy2018_col}{start_row}:{fy2018_col}{end_row})'
    sheet[f'{fy2017_col}{equity_total}'] = f'=SUM({fy2017_col}{start_row}:{fy2017_col}{end_row})'
    
    #B. Assets
    assets_total = row_mapping.get('assets_total')
    
    start_row = data_config.fs_assets_mapping.get('start_row')
    end_row = data_config.fs_assets_mapping.get('end_row')
    
    sheet[f'{fy2019_col}{assets_total}'] = f'=SUM({fy2019_col}{start_row}:{fy2019_col}{end_row})'
    sheet[f'{fy2018_col}{assets_total}'] = f'=SUM({fy2018_col}{start_row}:{fy2018_col}{end_row})'
    sheet[f'{fy2017_col}{assets_total}'] = f'=SUM({fy2017_col}{start_row}:{fy2017_col}{end_row})'
    
    ##2. Profit & Loss Account
    
    #Total Revenue
    total_revenue = row_mapping.get('total_revenue')
    
    start_row = data_config.fs_total_revenue_mapping.get('start_row')
    end_row = data_config.fs_total_revenue_mapping.get('end_row')
    
    sheet[f'{fy2019_col}{total_revenue}'] = f'=SUM({fy2019_col}{start_row}:{fy2019_col}{end_row})'
    sheet[f'{fy2018_col}{total_revenue}'] = f'=SUM({fy2018_col}{start_row}:{fy2018_col}{end_row})'
    sheet[f'{fy2017_col}{total_revenue}'] = f'=SUM({fy2017_col}{start_row}:{fy2017_col}{end_row})'
    
    #Total Expenses
    expenses_total = row_mapping.get('expenses_total')
    
    start_row = data_config.fs_expenses_mapping.get('start_row')
    end_row = data_config.fs_expenses_mapping.get('end_row')
    
    
    sheet[f'{fy2019_col}{expenses_total}'] = f'=SUM({fy2019_col}{start_row}:{fy2019_col}{end_row})'
    sheet[f'{fy2018_col}{expenses_total}'] = f'=SUM({fy2018_col}{start_row}:{fy2018_col}{end_row})'
    sheet[f'{fy2017_col}{expenses_total}'] = f'=SUM({fy2017_col}{start_row}:{fy2017_col}{end_row})'
    
    #Profit before Interest
    profit_before_interest = row_mapping.get('profit_before_interest')
    
    total_revenue = row_mapping.get('total_revenue')
    expenses_total = row_mapping.get('expenses_total')
    
    
    sheet[f'{fy2019_col}{profit_before_interest}'] = f'={fy2019_col}{total_revenue}-{fy2019_col}{expenses_total}'
    sheet[f'{fy2018_col}{profit_before_interest}'] = f'={fy2018_col}{total_revenue}-{fy2018_col}{expenses_total}'
    sheet[f'{fy2017_col}{profit_before_interest}'] = f'={fy2017_col}{total_revenue}-{fy2017_col}{expenses_total}'
    
    #Profit before ExtraOrdinary Items
    profit_before_extraordinary_items = row_mapping.get('profit_before_extraordinary_items')
    
    start_row = data_config.fs_profit_before_extraordinary_items_mapping.get('start_row')
    end_row = data_config.fs_profit_before_extraordinary_items_mapping.get('end_row')
    
    
    sheet[f'{fy2019_col}{profit_before_extraordinary_items}'] = f'={fy2019_col}{profit_before_interest}-SUM({fy2019_col}{start_row}:{fy2019_col}{end_row})'
    sheet[f'{fy2018_col}{profit_before_extraordinary_items}'] = f'={fy2018_col}{profit_before_interest}-SUM({fy2018_col}{start_row}:{fy2018_col}{end_row})'
    sheet[f'{fy2017_col}{profit_before_extraordinary_items}'] = f'={fy2017_col}{profit_before_interest}-SUM({fy2017_col}{start_row}:{fy2017_col}{end_row})'
    
    #Profit before Tax
    profit_before_tax = row_mapping.get('profit_before_tax')
    
    arg2 = data_config.fs_profit_before_tax.get('arg2')
    
    sheet[f'{fy2019_col}{profit_before_tax}'] = f'={fy2019_col}{profit_before_extraordinary_items}-{fy2019_col}{arg2}'
    sheet[f'{fy2018_col}{profit_before_tax}'] = f'={fy2018_col}{profit_before_extraordinary_items}-{fy2018_col}{arg2}'
    sheet[f'{fy2017_col}{profit_before_tax}'] = f'={fy2017_col}{profit_before_extraordinary_items}-{fy2017_col}{arg2}'
    
    #Profit(Loss) for the period from Continuing Operations IX-X
    profit_from_continuing_operations = row_mapping.get('profit_from_continuing_operations')
    
    start_row = data_config.fs_profit_from_continuing_operations.get('start_row')
    end_row = data_config.fs_profit_from_continuing_operations.get('end_row')
    
    sheet[f'{fy2019_col}{profit_from_continuing_operations}'] = f'={fy2019_col}{profit_before_tax}-SUM({fy2019_col}{start_row}:{fy2019_col}{end_row})'
    sheet[f'{fy2018_col}{profit_from_continuing_operations}'] = f'={fy2018_col}{profit_before_tax}-SUM({fy2018_col}{start_row}:{fy2018_col}{end_row})'
    sheet[f'{fy2017_col}{profit_from_continuing_operations}'] = f'={fy2017_col}{profit_before_tax}-SUM({fy2017_col}{start_row}:{fy2017_col}{end_row})'


    #Profit(Loss) for the period from Continuing Operations XII-XIII
    profit_discontinuing_op_after_tax = row_mapping.get('profit_discontinuing_op_after_tax')
    
    arg1 = data_config.fs_profit_discontinuing_op_after_tax.get('arg1')
    arg2 = data_config.fs_profit_discontinuing_op_after_tax.get('arg2')
    
    sheet[f'{fy2019_col}{profit_discontinuing_op_after_tax}'] = f'={fy2019_col}{arg1}-{fy2019_col}{arg2}'
    sheet[f'{fy2018_col}{profit_discontinuing_op_after_tax}'] = f'={fy2018_col}{arg1}-{fy2018_col}{arg2}'
    sheet[f'{fy2017_col}{profit_discontinuing_op_after_tax}'] = f'={fy2017_col}{arg1}-{fy2017_col}{arg2}'
    
    #Profit/Loss for the Period or After Tax
    profit_loss_period_or_profit_after_tax = row_mapping.get('profit_loss_period_or_profit_after_tax')
    
    
    sheet[f'{fy2019_col}{profit_loss_period_or_profit_after_tax}'] = f'={fy2019_col}{profit_discontinuing_op_after_tax}+{fy2019_col}{profit_from_continuing_operations}'
    sheet[f'{fy2018_col}{profit_loss_period_or_profit_after_tax}'] = f'={fy2018_col}{profit_discontinuing_op_after_tax}+{fy2018_col}{profit_from_continuing_operations}'
    sheet[f'{fy2017_col}{profit_loss_period_or_profit_after_tax}'] = f'={fy2017_col}{profit_discontinuing_op_after_tax}+{fy2017_col}{profit_from_continuing_operations}'
    
    #Amount Available for Operations
    amount_avail_for_approp = row_mapping.get('amount_avail_for_approp')
    
    arg2 = data_config.fs_amount_avail_for_approp.get('arg2')
    
    sheet[f'{fy2019_col}{amount_avail_for_approp}'] = f'={fy2019_col}{profit_loss_period_or_profit_after_tax}+{fy2019_col}{arg2}'
    sheet[f'{fy2018_col}{amount_avail_for_approp}'] = f'={fy2018_col}{profit_loss_period_or_profit_after_tax}+{fy2018_col}{arg2}'
    sheet[f'{fy2017_col}{amount_avail_for_approp}'] = f'={fy2017_col}{profit_loss_period_or_profit_after_tax}+{fy2017_col}{arg2}'

    #Debt/Equity
    debt_or_equity = row_mapping.get('debt_or_equity')
    
    start_row1 = data_config.fs_debt_or_equity.get('start_row1')
    end_row1 = data_config.fs_debt_or_equity.get('end_row1')
    start_row2 = data_config.fs_debt_or_equity.get('start_row2')
    end_row2 = data_config.fs_debt_or_equity.get('end_row2')
    
    
    sheet[f'{fy2019_col}{debt_or_equity}'] = f'=({fy2019_col}{start_row1}+{fy2019_col}{end_row1})/SUM({fy2019_col}{start_row2}:{fy2019_col}{end_row2})'
    sheet[f'{fy2018_col}{debt_or_equity}'] = f'=({fy2018_col}{start_row1}+{fy2018_col}{end_row1})/SUM({fy2018_col}{start_row2}:{fy2018_col}{end_row2})'
    sheet[f'{fy2017_col}{debt_or_equity}'] = f'=({fy2017_col}{start_row1}+{fy2017_col}{end_row1})/SUM({fy2017_col}{start_row2}:{fy2017_col}{end_row2})'
    
    #Current Ratio
    current_ratio = row_mapping.get('current_ratio')
    
    start_row1 = data_config.fs_current_ratio.get('start_row1')
    end_row1 = data_config.fs_current_ratio.get('end_row1')
    start_row2 = data_config.fs_current_ratio.get('start_row2')
    end_row2 = data_config.fs_current_ratio.get('end_row2')
    
    sheet[f'{fy2019_col}{current_ratio}'] = f'=SUM({fy2019_col}{start_row1}:{fy2019_col}{end_row1})/SUM({fy2019_col}{start_row2}:{fy2019_col}{end_row2})'
    sheet[f'{fy2018_col}{current_ratio}'] = f'=SUM({fy2018_col}{start_row1}:{fy2018_col}{end_row1})/SUM({fy2018_col}{start_row2}:{fy2018_col}{end_row2})'
    sheet[f'{fy2017_col}{current_ratio}'] = f'=SUM({fy2017_col}{start_row1}:{fy2017_col}{end_row1})/SUM({fy2017_col}{start_row2}:{fy2017_col}{end_row2})'
    
    #Stock Days
    stock_days = row_mapping.get('stock_days')
    
    arg1 = data_config.fs_stock_days.get('arg1')
    arg2 = data_config.fs_stock_days.get('arg2')
    
    sheet[f'{fy2019_col}{stock_days}'] = f'=365/({fy2019_col}{arg1}/{fy2019_col}{arg2})'
    sheet[f'{fy2018_col}{stock_days}'] = f'=365/({fy2018_col}{arg1}/{fy2018_col}{arg2})'
    sheet[f'{fy2017_col}{stock_days}'] = f'=365/({fy2017_col}{arg1}/{fy2017_col}{arg2})'
    
    #Debtor Days
    debtor_days = row_mapping.get('debtor_days')
    
    arg1 = data_config.fs_debtor_days.get('arg1')
    arg2 = data_config.fs_debtor_days.get('arg2')
    
    sheet[f'{fy2019_col}{debtor_days}'] = f'=365/({fy2019_col}{arg1}/{fy2019_col}{arg2})'
    sheet[f'{fy2018_col}{debtor_days}'] = f'=365/({fy2018_col}{arg1}/{fy2018_col}{arg2})'
    sheet[f'{fy2017_col}{debtor_days}'] = f'=365/({fy2017_col}{arg1}/{fy2017_col}{arg2})'
    
    #Creditor Days
    creditor_days = row_mapping.get('creditor_days')
    
    arg1 = data_config.fs_creditor_days.get('arg1')
    arg2 = data_config.fs_creditor_days.get('arg2')
    
    sheet[f'{fy2019_col}{creditor_days}'] = f'=365/({fy2019_col}{arg1}/{fy2019_col}{arg2})'
    sheet[f'{fy2018_col}{creditor_days}'] = f'=365/({fy2018_col}{arg1}/{fy2018_col}{arg2})'
    sheet[f'{fy2017_col}{creditor_days}'] = f'=365/({fy2017_col}{arg1}/{fy2017_col}{arg2})'
    
    #Operating Cycle
    operating_cycle = row_mapping.get('operating_cycle')
    
    arg1 = data_config.fs_operating_cycle.get('arg1')
    arg2 = data_config.fs_operating_cycle.get('arg2')
    arg3 = data_config.fs_operating_cycle.get('arg3')

    
    sheet[f'{fy2019_col}{operating_cycle}'] = f'={fy2019_col}{arg1}+{fy2019_col}{arg2}-{fy2019_col}{arg3}'
    sheet[f'{fy2018_col}{operating_cycle}'] = f'={fy2018_col}{arg1}+{fy2018_col}{arg2}-{fy2018_col}{arg3}'
    sheet[f'{fy2017_col}{operating_cycle}'] = f'={fy2017_col}{arg1}+{fy2017_col}{arg2}-{fy2017_col}{arg3}'
    
    #Working Capital Requirement
    working_capital_requirement = row_mapping.get('working_capital_requirement')
    
    arg1 = data_config.fs_working_capital_requirement.get('arg1')
    arg2 = data_config.fs_working_capital_requirement.get('arg2')
    
    sheet[f'{fy2019_col}{working_capital_requirement}'] = f'={fy2019_col}{arg1}/360*{fy2019_col}{arg2}'
    sheet[f'{fy2018_col}{working_capital_requirement}'] = f'={fy2018_col}{arg1}/360*{fy2018_col}{arg2}'
    sheet[f'{fy2017_col}{working_capital_requirement}'] = f'={fy2017_col}{arg1}/360*{fy2017_col}{arg2}'
    
    #Net Working Capital Requirement
    net_working_capital_reuirement = row_mapping.get('net_working_capital_reuirement')
    
    arg1 = data_config.fs_net_working_capital_reuirement.get('arg1')
    arg2 = data_config.fs_net_working_capital_reuirement.get('arg2')
    
    sheet[f'{fy2019_col}{net_working_capital_reuirement}'] = f'={fy2019_col}{arg1}-{fy2019_col}{arg2}'
    sheet[f'{fy2018_col}{net_working_capital_reuirement}'] = f'={fy2018_col}{arg1}-{fy2018_col}{arg2}'
    sheet[f'{fy2017_col}{net_working_capital_reuirement}'] = f'={fy2017_col}{arg1}-{fy2017_col}{arg2}'
    
    #Interest Coverage Ratio
    interest_coverage_ratio = row_mapping.get('interest_coverage_ratio')
    
    arg1 = data_config.fs_interest_coverage_ratio.get('arg1')
    arg2 = data_config.fs_interest_coverage_ratio.get('arg2')
    
    sheet[f'{fy2019_col}{interest_coverage_ratio}'] = f'={fy2019_col}{arg1}/{fy2019_col}{arg2}'
    sheet[f'{fy2018_col}{interest_coverage_ratio}'] = f'={fy2018_col}{arg1}/{fy2018_col}{arg2}'
    sheet[f'{fy2017_col}{interest_coverage_ratio}'] = f'={fy2017_col}{arg1}/{fy2017_col}{arg2}'
    
    #DSCR
    dscr = row_mapping.get('dscr')
    
    arg1 = data_config.fs_dscr.get('arg1')
    arg2 = data_config.fs_dscr.get('arg2')
    
    sheet[f'{fy2019_col}{dscr}'] = f'={fy2019_col}{arg1}/{fy2019_col}{arg2}'
    
    return sheet
    
def revenue_estimation_formulas(sheet):
    
    #Growth rate
    GR_2019 = data_config.revenue_estimation_col_mapping.get('GR_2019')
    GR_2020 = data_config.revenue_estimation_col_mapping.get('GR_2020')
    Year2018 = data_config.revenue_estimation_col_mapping.get('Year2018')
    
    calc_row = data_config.RE_growth_rate_yearly.get('calc_row')
    result_row = data_config.RE_growth_rate_yearly.get('result_row')
    
    
    sheet[f'{GR_2019}{result_row}'] = f'={GR_2019}{calc_row}/{Year2018}{calc_row}-1'
    sheet[f'{GR_2020}{result_row}'] = f'={GR_2020}{calc_row}/{GR_2019}{calc_row}-1'
    
    Year2020 = data_config.revenue_estimation_col_mapping.get('Year2020')
    Year2019 = data_config.revenue_estimation_col_mapping.get('Year2019')
    total_calc_row = data_config.RE_growth_rate_mom_trend.get('total_calc_row')
    estimated_2022 = data_config.revenue_estimation_col_mapping.get('estimated_2022')
    
    #sheet[f'{Year2020}{calc_row}'] = f'={Year2019}{total_calc_row}'
    sheet[f'{estimated_2022}{calc_row}'] = f'={Year2020}{calc_row}*(1+{estimated_2022}{result_row})'
    
    
    estimated_2021 = data_config.revenue_estimation_col_mapping.get('estimated_2021')
    
    start_row = data_config.RE_growth_rate_mom_trend.get('start_row')
    total_mom_row = start_row + 12
    
    sheet[f'{Year2020}{calc_row}'] = f'={Year2019}{total_mom_row}'
    sheet[f'{estimated_2021}{calc_row}'] = f'={GR_2020}{total_mom_row}'
    sheet[f'{estimated_2022}{calc_row}'] = f'={GR_2020}{calc_row}*(1+{estimated_2022}{result_row})'
    
    ## MOM Revenue Trend
    
    #Best Estimate for FY2020-21
    fy2019_20 = data_config.revenue_estimation_col_mapping.get('fy2019_20')
    fy2020_21_estimate = data_config.revenue_estimation_col_mapping.get('fy2020_21_estimate')
    growth_rate = data_config.revenue_estimation_col_mapping.get('growth_rate')
    estimated_2021 = data_config.revenue_estimation_col_mapping.get('estimated_2021')
    result_row = data_config.RE_growth_rate_yearly.get('result_row')
    
    start_row = data_config.RE_growth_rate_mom_trend.get('start_row')
    row = start_row+7
    
    i=5
    while i>0:
        sheet[f'{growth_rate}{row}'] = f'={fy2020_21_estimate}{row}*(1+${estimated_2021}${result_row})'
        row=row+1
        i=i-1
    # sheet[f'{fy2020_21_estimate}{row}'] = f'={fy2019_20}{row}*(1+$F$10)'
    # row +=1
    # sheet[f'{fy2020_21_estimate}{row}'] = f'={fy2019_20}{row}*(1+$F$10)'
    # row +=1
    # sheet[f'{fy2020_21_estimate}{row}'] = f'={fy2019_20}{row}*(1+$F$10)'
    # row +=1
    # sheet[f'{fy2020_21_estimate}{row}'] = f'={fy2019_20}{row}*(1+$F$10)'
    
    
    #Total
    start_total = start_row
    end_total = start_row + 11
    
    # sheet[f'{fy2019_20}29'] = f'=SUM({fy2019_20}{start_total}:{fy2019_20}{end_total})'
    # sheet[f'{fy2020_21_estimate}29'] = f'=SUM({fy2020_21_estimate}{start_total}:{fy2020_21_estimate}{end_total})'
    # sheet['E29'] = '=(D29/C29)-1'
    
    growth_rate = data_config.revenue_estimation_col_mapping.get('growth_rate')
    fy2019_20 = data_config.revenue_estimation_col_mapping.get('fy2019_20')
    fy2020_21_estimate = data_config.revenue_estimation_col_mapping.get('fy2020_21_estimate')
    
    start_row = data_config.RE_growth_rate_mom_trend.get('start_row')
    row = start_row
    i=13
    while i>0:
        #sheet[f'{estimated_2021}{row}'] = f'=({growth_rate}{row}/{fy2020_21_estimate}{row})-1'
        # sheet[f'{estimated_2021}{row}'] = f'=({growth_rate}{row}/{fy2020_21_estimate}{row})-1'  --->> =(E17/D17)-1
        # new formula =IFERROR((D17-E17)/D17,"NA")
        sheet[f'{estimated_2021}{row}'] = f'=IFERROR(( {fy2020_21_estimate}{row} - {growth_rate}{row} )/{fy2020_21_estimate}{row}, "NA")'
        row +=1
        i=i-1
    
    total_calc_row = data_config.RE_growth_rate_mom_trend.get('total_calc_row')
    
    sheet[f'{fy2019_20}{total_calc_row}'] = f'=SUM({fy2019_20}{start_total}:{fy2019_20}{end_total})'
    sheet[f'{fy2020_21_estimate}{total_calc_row}'] = f'=SUM({fy2020_21_estimate}{start_total}:{fy2020_21_estimate}{end_total})'
    sheet[f'{growth_rate}{total_calc_row}'] = f'=SUM({growth_rate}{start_total}:{growth_rate}{end_total})'
    sheet[f'{estimated_2021}{total_calc_row}'] = f'=({growth_rate}{total_calc_row}/{fy2020_21_estimate}{total_calc_row})-1'
    
    
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    # row +=1
    # sheet[f'{growth_rate}{row}'] = f'=({fy2020_21_estimate}{row}/{fy2019_20}{row})-1'
    
    #Turnover Tringulation
    turnover_tringulation = data_config.revenue_estimation_col_mapping.get('turnover_tringulation')
    
    turnover_as_per_books = data_config.RE_turnover_tringulation.get('turnover_as_per_books')
    turnover_as_per_gst = data_config.RE_turnover_tringulation.get('turnover_as_per_gst')
    banking_credits = data_config.RE_turnover_tringulation.get('banking_credits')
    gst_book_ratio = data_config.RE_turnover_tringulation.get('gst_book_ratio')
    banking_book_ratio = data_config.RE_turnover_tringulation.get('banking_book_ratio')
    banking_gst_ratio = data_config.RE_turnover_tringulation.get('banking_gst_ratio')
    
    total_CT_for_current_acc = data_config.RE_turnover_tringulation.get('total_CT_for_current_acc')
    total_CT_for_oc_od_acc = data_config.RE_turnover_tringulation.get('total_CT_for_oc_od_acc')
    
    sheet[f'{turnover_tringulation}{banking_credits}'] = f'={turnover_tringulation}{total_CT_for_current_acc}+{turnover_tringulation}{total_CT_for_oc_od_acc}'
    sheet[f'{turnover_tringulation}{gst_book_ratio}'] = f'=IFERROR({turnover_tringulation}{turnover_as_per_gst}/{turnover_tringulation}{turnover_as_per_books}, 0)'
    sheet[f'{turnover_tringulation}{banking_book_ratio}'] = f'=IFERROR({turnover_tringulation}{banking_credits}/{turnover_tringulation}{turnover_as_per_books}, 0)'
    sheet[f'{turnover_tringulation}{banking_gst_ratio}'] = f'=IFERROR({turnover_tringulation}{banking_credits}/{turnover_tringulation}{turnover_as_per_gst}, 0)'
    
    #Estimation of Margin Based on Previous Data
    
    Year2018 = data_config.revenue_estimation_col_mapping.get('Year2018')
    Year2019 = data_config.revenue_estimation_col_mapping.get('Year2019')
    Year2020 = data_config.revenue_estimation_col_mapping.get('Year2020')
    
    sales_turnover = data_config.RE_margin_estimation_EBITDA.get('sales_turnover')
    sales_2020 = data_config.RE_margin_estimation_EBITDA.get('sales_2020')
    
    sheet[f'{Year2018}{sales_turnover}'] = f'={Year2020}{sales_2020}'
    sheet[f'{Year2019}{sales_turnover}'] = f'={Year2018}{sales_turnover}'
    sheet[f'{Year2020}{sales_turnover}'] = f'={Year2019}{sales_turnover}'
    
    obligations = data_config.RE_margin_estimation_EBITDA.get('obligations')
    foir = data_config.RE_margin_estimation_EBITDA.get('foir')
    EBITDA = data_config.RE_margin_estimation_EBITDA.get('EBITDA')
    
    sheet[f'{Year2018}{EBITDA}'] = f'={Year2018}{obligations}/{Year2018}{foir}'
    sheet[f'{Year2019}{EBITDA}'] = f'={Year2019}{obligations}/{Year2019}{foir}'
    sheet[f'{Year2020}{EBITDA}'] = f'={Year2020}{obligations}/{Year2020}{foir}'
    
    EBITDA_margin = data_config.RE_margin_estimation_EBITDA.get('EBITDA_margin')
    
    sheet[f'{Year2018}{EBITDA_margin}'] = f'={Year2018}{EBITDA}/{Year2018}{sales_turnover}'
    sheet[f'{Year2019}{EBITDA_margin}'] = f'={Year2019}{EBITDA}/{Year2019}{sales_turnover}'
    sheet[f'{Year2020}{EBITDA_margin}'] = f'={Year2020}{EBITDA}/{Year2020}{sales_turnover}'
    
    return sheet
    

def gtp_lap_bl_formulas(sheet):
    
    col_mapping = data_config.gtp_lap_bl_col_mapping
    row_mapping = data_config.gtp_lap_bl_row_mapping
    
    Year2018 = col_mapping.get('Year2018')
    Year2019 = col_mapping.get('Year2019')
    Year2020 = col_mapping.get('Year2020')
    Year2020_21_no_loan = col_mapping.get('Year2020_21_no_loan')
    Year2021_22_no_loan = col_mapping.get('Year2021_22_no_loan')
    Year2020_21_with_loan = col_mapping.get('Year2020_21_with_loan')
    Year2021_22_with_loan = col_mapping.get('Year2021_22_with_loan')
    
    #Sales Turnover
    
    sales_turnover= row_mapping.get('sales_turnover')
    
    Y2018_RE = data_config.revenue_estimation_col_mapping.get('Year2018')
    Y2019_RE = data_config.revenue_estimation_col_mapping.get('Year2019')
    Y2020_RE = data_config.revenue_estimation_col_mapping.get('Year2020')
    Y2021_RE = data_config.revenue_estimation_col_mapping.get('estimated_2021')
    Y2022_RE = data_config.revenue_estimation_col_mapping.get('estimated_2022')
    
    calc_row = data_config.RE_growth_rate_yearly.get('calc_row')
    
    sheet[f'{Year2018}{sales_turnover}'] = f'=revenue_estimation!{Y2018_RE}{calc_row}'
    sheet[f'{Year2019}{sales_turnover}'] = f'=revenue_estimation!{Y2019_RE}{calc_row}'
    sheet[f'{Year2020}{sales_turnover}'] = f'=revenue_estimation!{Y2020_RE}{calc_row}'
    sheet[f'{Year2020_21_no_loan}{sales_turnover}'] = f'=revenue_estimation!{Y2021_RE}{calc_row}'
    sheet[f'{Year2021_22_no_loan}{sales_turnover}'] = f'=revenue_estimation!{Y2022_RE}{calc_row}'
    
    sheet[f'{Year2020_21_with_loan}{sales_turnover}'] = f'={Year2020_21_no_loan}{sales_turnover}'
    sheet[f'{Year2021_22_with_loan}{sales_turnover}'] = f'={Year2021_22_no_loan}{sales_turnover}'
    
    #Net Profit After Tax
    
    profit_after_tax = row_mapping.get('profit_after_tax')
    
    profit_before_tax = data_config.gtp_lap_bl_profit_after_tax.get('profit_before_tax')
    non_buss_income = data_config.gtp_lap_bl_profit_after_tax.get('non_buss_income')
    tax_paid = data_config.gtp_lap_bl_profit_after_tax.get('tax_paid')
    
    sheet[f'{Year2018}{profit_after_tax}'] = f'={Year2018}{profit_before_tax}-{Year2018}{non_buss_income}-{Year2018}{tax_paid}'
    sheet[f'{Year2019}{profit_after_tax}'] = f'={Year2019}{profit_before_tax}-{Year2019}{non_buss_income}-{Year2019}{tax_paid}'
    sheet[f'{Year2020}{profit_after_tax}'] = f'={Year2020}{profit_before_tax}-{Year2020}{non_buss_income}-{Year2020}{tax_paid}'
    
    #Earning Before Interest Depreciation and Amortisation(Actuals)
    
    earnings_actuals = row_mapping.get('earnings_actuals')
    
    sr1 = data_config.gtp_lap_bl_earning_actuals.get('sr1')
    er1 = data_config.gtp_lap_bl_earning_actuals.get('er1')
    sr2 = data_config.gtp_lap_bl_earning_actuals.get('sr2')
    er2 = data_config.gtp_lap_bl_earning_actuals.get('er2')
    arg = data_config.gtp_lap_bl_earning_actuals.get('arg')
    
    sheet[f'{Year2018}{earnings_actuals}'] = f'=SUM({Year2018}{sr1}:{Year2018}{er1}, {Year2018}{sr2}:{Year2018}{er2})-{Year2018}{arg}'
    sheet[f'{Year2019}{earnings_actuals}'] = f'=SUM({Year2019}{sr1}:{Year2019}{er1}, {Year2019}{sr2}:{Year2019}{er2})-{Year2018}{arg}'
    sheet[f'{Year2020}{earnings_actuals}'] = f'=SUM({Year2020}{sr1}:{Year2020}{er1}, {Year2020}{sr2}:{Year2020}{er2})-{Year2020}{arg}'
    
    sheet[f'{Year2020_21_no_loan}{earnings_actuals}'] = f'={Year2020_21_no_loan}{sales_turnover}*({Year2020}{earnings_actuals}/{Year2020}{sales_turnover})'
    sheet[f'{Year2021_22_no_loan}{earnings_actuals}'] = f'={Year2021_22_no_loan}{sales_turnover}*({Year2020_21_no_loan}{earnings_actuals}/{Year2020_21_no_loan}{sales_turnover})'
    sheet[f'{Year2020_21_with_loan}{earnings_actuals}'] = f'={Year2020_21_with_loan}{sales_turnover}*(${Year2020}${earnings_actuals}/${Year2020}${sales_turnover})'
    sheet[f'{Year2021_22_with_loan}{earnings_actuals}'] = f'={Year2021_22_with_loan}{sales_turnover}*(${Year2020}${earnings_actuals}/${Year2020}${sales_turnover})'
    
    
    earnings_considered = row_mapping.get('earnings_considered')
    multiplier = row_mapping.get('multiplier')
    margin = row_mapping.get('margin')
    EBITDA_margin = row_mapping.get('EBITDA_margin')
    
    sheet[f'{Year2020_21_no_loan}{earnings_considered}'] = f'={Year2020_21_no_loan}{sales_turnover}*IF(({Year2020}{earnings_actuals}/{Year2020}{sales_turnover})*{Year2020_21_no_loan}{multiplier}>{Year2018}{margin},{Year2018}{margin},({Year2020}{earnings_actuals}/{Year2020}{sales_turnover})*{Year2020_21_no_loan}{multiplier})'
    sheet[f'{Year2021_22_no_loan}{earnings_considered}'] = f'={Year2021_22_no_loan}{sales_turnover}*${Year2020_21_no_loan}${EBITDA_margin}'
    
    sheet[f'{Year2020_21_with_loan}{earnings_considered}'] = f'={Year2020_21_with_loan}{sales_turnover}*(${Year2020_21_no_loan}${EBITDA_margin})'
    sheet[f'{Year2021_22_with_loan}{earnings_considered}'] = f'={Year2021_22_with_loan}{sales_turnover}*(${Year2020_21_no_loan}${earnings_considered}/${Year2020_21_no_loan}${sales_turnover})'
    
    
    #Any Other Income
    
    other_income = row_mapping.get('other_income')
    total_other_income = row_mapping.get('total_other_income')
    
    sheet[f'{Year2020_21_no_loan}{other_income}'] = f'={Year2018}{total_other_income}'
    sheet[f'{Year2021_22_no_loan}{other_income}'] = f'={Year2019}{total_other_income}'
    sheet[f'{Year2020_21_with_loan}{other_income}'] = f'={Year2018}{total_other_income}'
    sheet[f'{Year2021_22_with_loan}{other_income}'] = f'={Year2019}{total_other_income}'
    
    
    #Other Obligations
    
    other_obligations = row_mapping.get('other_obligations')
    tenure = row_mapping.get('tenure')
    rate = row_mapping.get('rate')
    amount = row_mapping.get('amount')
    rem_months = row_mapping.get('rem_months')
    
    row_number_total = data_config.rtr_sheet_statics.get('row_number_total')
    amount_2020_21 = data_config.rtr_sheet_statics.get('amount_2020_21')
    amount_2021_22 = data_config.rtr_sheet_statics.get('amount_2021_22')
    
    sheet[f'{Year2020_21_no_loan}{other_obligations}'] = f'=RTR!{amount_2020_21}{row_number_total}'
    sheet[f'{Year2021_22_no_loan}{other_obligations}'] = f'=RTR!{amount_2021_22}{row_number_total}'
    sheet[f'{Year2020_21_with_loan}{other_obligations}'] = f'={Year2020_21_no_loan}{other_obligations}+PMT({Year2021_22_with_loan}{rate}/12,{Year2021_22_with_loan}{tenure},-{Year2021_22_with_loan}{amount},0)*{Year2021_22_with_loan}{rem_months}'
    sheet[f'{Year2021_22_with_loan}{other_obligations}'] = f'={Year2021_22_no_loan}{other_obligations}+PMT({Year2021_22_with_loan}{rate}/12,{Year2021_22_with_loan}{tenure},-{Year2021_22_with_loan}{amount})*12'
    
    #GSFT EMI
    
    gsft_emi = row_mapping.get('gsft_emi')
    
    sheet[f'{Year2020_21_no_loan}{gsft_emi}'] = f'=-PMT({Year2020_21_no_loan}{amount}/12,{Year2020_21_no_loan}{rate},{Year2020_21_no_loan}{rem_months},0)*{Year2021_22_with_loan}{rem_months}'
    sheet[f'{Year2021_22_no_loan}{gsft_emi}'] = f'=-PMT({Year2021_22_no_loan}{amount}/12,{Year2021_22_no_loan}{rate},{Year2021_22_no_loan}{rem_months},0)*12'
    sheet[f'{Year2020_21_with_loan}{gsft_emi}'] = f'={Year2020_21_no_loan}{gsft_emi}'
    sheet[f'{Year2021_22_with_loan}{gsft_emi}'] = f'={Year2021_22_no_loan}{gsft_emi}'
    
    #FOIR
    
    foir1 = row_mapping.get('foir1')
    
    sheet[f'{Year2020_21_no_loan}{foir1}'] = f'=(SUM({Year2020_21_no_loan}{other_obligations}:{Year2020_21_no_loan}{gsft_emi}))/SUM({Year2020_21_no_loan}{earnings_considered}:{Year2020_21_no_loan}{other_income})'
    sheet[f'{Year2021_22_no_loan}{foir1}'] = f'=(SUM({Year2021_22_no_loan}{other_obligations}:{Year2021_22_no_loan}{gsft_emi}))/SUM({Year2021_22_no_loan}{earnings_considered}:{Year2021_22_no_loan}{other_income})'
    sheet[f'{Year2020_21_with_loan}{foir1}'] = f'=(SUM({Year2020_21_with_loan}{other_obligations}:{Year2020_21_with_loan}{gsft_emi}))/SUM({Year2020_21_with_loan}{earnings_considered}:{Year2020_21_with_loan}{other_income})'
    sheet[f'{Year2021_22_with_loan}{foir1}'] = f'=(SUM({Year2021_22_with_loan}{other_obligations}:{Year2021_22_with_loan}{gsft_emi}))/SUM({Year2021_22_with_loan}{earnings_considered}:{Year2021_22_with_loan}{other_income})'
    
    #EBITDA Margin
    
    EBITDA_margin = row_mapping.get('EBITDA_margin')
    
    sheet[f'{Year2018}{EBITDA_margin}'] = f'={Year2018}{earnings_actuals}/{Year2018}{sales_turnover}'
    sheet[f'{Year2019}{EBITDA_margin}'] = f'={Year2019}{earnings_actuals}/{Year2019}{sales_turnover}'
    sheet[f'{Year2020}{EBITDA_margin}'] = f'={Year2020}{earnings_actuals}/{Year2020}{sales_turnover}'
    sheet[f'{Year2020_21_no_loan}{EBITDA_margin}'] = f'={Year2020_21_no_loan}{earnings_considered}/{Year2020_21_no_loan}{sales_turnover}'
    
    
    #DSCR at EBITDA
    
    DSCR_at_EBIDA = row_mapping.get('DSCR_at_EBIDA')
    
    sheet[f'{Year2020_21_no_loan}{DSCR_at_EBIDA}'] = f'=1/{Year2020_21_no_loan}{foir1}'
    sheet[f'{Year2021_22_no_loan}{DSCR_at_EBIDA}'] = f'=1/{Year2021_22_no_loan}{foir1}'
    sheet[f'{Year2020_21_with_loan}{DSCR_at_EBIDA}'] = f'=1/{Year2020_21_with_loan}{foir1}'
    sheet[f'{Year2021_22_with_loan}{DSCR_at_EBIDA}'] = f'=1/{Year2021_22_with_loan}{foir1}'
    
    #Revenue Considered
    
    # revenue_considered = row_mapping.get('revenue_considered')
    # haircut_revenue = row_mapping.get('haircut_revenue')
    
    # sheet[f'{Year2020_21_no_loan}{revenue_considered}'] = f'={Year2020_21_no_loan}{sales_turnover}*(1-{Year2020_21_no_loan}{haircut_revenue})'
    # sheet[f'{Year2021_22_no_loan}{revenue_considered}'] = f'={Year2021_22_no_loan}{sales_turnover}*(1-{Year2021_22_no_loan}{haircut_revenue})'
    # sheet[f'{Year2020_21_with_loan}{revenue_considered}'] = f'={Year2020_21_with_loan}{sales_turnover}*(1-{Year2020_21_with_loan}{haircut_revenue})'
    # sheet[f'{Year2021_22_with_loan}{revenue_considered}'] = f'={Year2021_22_with_loan}{sales_turnover}*(1-{Year2021_22_with_loan}{haircut_revenue})'
    
    
    #EBITDA Considered
    
    # EBITDA_considered = row_mapping.get('EBITDA_considered')
    # haircut_margin = row_mapping.get('haircut_margin')
    
    # sheet[f'{Year2020_21_no_loan}{EBITDA_considered}'] = f'={Year2020_21_no_loan}{revenue_considered}*${Year2020_21_no_loan}${EBITDA_margin}*(1-{Year2020_21_no_loan}{haircut_margin})'
    # sheet[f'{Year2021_22_no_loan}{EBITDA_considered}'] = f'={Year2021_22_no_loan}{revenue_considered}*${Year2020_21_no_loan}${EBITDA_margin}*(1-{Year2021_22_no_loan}{haircut_margin})'
    # sheet[f'{Year2020_21_with_loan}{EBITDA_considered}'] = f'={Year2020_21_with_loan}{revenue_considered}*${Year2020_21_no_loan}${EBITDA_margin}*(1-{Year2020_21_with_loan}{haircut_margin})'
    # sheet[f'{Year2021_22_with_loan}{EBITDA_considered}'] = f'={Year2021_22_with_loan}{revenue_considered}*${Year2020_21_no_loan}${EBITDA_margin}*(1-{Year2021_22_with_loan}{haircut_margin})'
    
    #FOIR
    
    # foir2 = row_mapping.get('foir2')
    
    # sheet[f'{Year2020_21_no_loan}{foir2}'] = f'=SUM({Year2020_21_no_loan}{other_obligations}:{Year2020_21_no_loan}{gsft_emi})/{Year2020_21_no_loan}{EBITDA_considered}'
    # sheet[f'{Year2021_22_no_loan}{foir2}'] = f'=SUM({Year2021_22_no_loan}{other_obligations}:{Year2021_22_no_loan}{gsft_emi})/{Year2021_22_no_loan}{EBITDA_considered}'
    # sheet[f'{Year2020_21_with_loan}{foir2}'] = f'=SUM({Year2020_21_with_loan}{other_obligations}:{Year2020_21_with_loan}{gsft_emi})/{Year2020_21_with_loan}{EBITDA_considered}'
    # sheet[f'{Year2021_22_with_loan}{foir2}'] = f'=SUM({Year2021_22_with_loan}{other_obligations}:{Year2021_22_with_loan}{gsft_emi})/{Year2021_22_with_loan}{EBITDA_considered}'
    
    return sheet
    
def rtr_formulas(sheet):
    
    col_mapping = data_config.rtr_col_mapping
    row_mapping = data_config.rtr_row_mapping
    
    year = col_mapping.get('year')
    amount = col_mapping.get('amount')
    loan_amount = col_mapping.get('loan_amount')
    pos = col_mapping.get('POS')
    
    y1 = row_mapping.get('y1')
    y2 = row_mapping.get('y2')
    y3 = row_mapping.get('y3')
    y4 = row_mapping.get('y4')
    y5 = row_mapping.get('y5')
    y6 = row_mapping.get('y6')
    
    sheet[f'{amount}{y1}'] = f'=SUMIF({pos}:{pos},{year}{y1},{loan_amount}:{loan_amount})'
    sheet[f'{amount}{y2}'] = f'=SUMIF({pos}:{pos},{year}{y2},{loan_amount}:{loan_amount})'
    sheet[f'{amount}{y3}'] = f'=SUMIF({pos}:{pos},{year}{y3},{loan_amount}:{loan_amount})'
    sheet[f'{amount}{y4}'] = f'=SUMIF({pos}:{pos},{year}{y4},{loan_amount}:{loan_amount})'
    sheet[f'{amount}{y5}'] = f'=SUMIF({pos}:{pos},{year}{y5},{loan_amount}:{loan_amount})'
    sheet[f'{amount}{y6}'] = f'=SUMIF({pos}:{pos},{year}{y6},{loan_amount}:{loan_amount})'
    
    return sheet
