import openpyxl
import msal
import time
import requests
import os
import re


#LOCAL IMPORTS
import update_reports.enums as enums
import update_reports.constants as constants
import update_reports.utilities as utilities

from commons.config import (CLIENT_ID, AUTHORITY_URL, USERNAME, PASSWORD, SCOPES, RESOURCE_URL, API_VERSION)
from commons.constants import uat_lead_code
from commons.utils import get_from_one_drive, push_to_one_drive



def validator(product):
    if not enums.Products.has_value(product):
        raise Exception('Invalid Product: {}'.format(product))
    
    if product not in constants.PRODUCT_PROGRAM_MAP:
        raise Exception('Invalid Product: {}'.format(product))


def get_cell_coordinates(coordinates, key):
    excel_axis = coordinates.get_by_key(key, coordinates)
    if excel_axis is None:
        return None
        
    cell_coordinates = excel_axis.column + str(excel_axis.row)
    print('Cell coordinate of {}: {}'.format(key,cell_coordinates))
    return cell_coordinates

def excel_reader(request, path, response_data):
    product = request['product']

    validator(product)
    wb = openpyxl.load_workbook(path, data_only=True, read_only= True)
    sheetNameArr = constants.PRODUCT_PROGRAM_MAP[product]
    
    for sheetName in sheetNameArr:
        print('sheet name: {}'.format(sheetName))
        keyName = re.sub(" ", '_', sheetName).upper()
        response_data[keyName] = {}
        sheet = wb[sheetName]
        subProgram = '{}_{}'.format(product,sheetName)
        coordinates = constants.PROGRAM_EXCEL_CELL_MAP[subProgram]
        for key in constants.READ_FIELDS:
            cell_coordinates = get_cell_coordinates(coordinates, key['name'])
            if cell_coordinates is None or sheet[cell_coordinates] is None:
                response_data[keyName][key['name']] = None
            else:
                val = sheet[cell_coordinates].value
                print('value: {}'.format(val))
                if isinstance(val, (int, float)):
                    response_data[keyName][key['name']] = (val * 100) if key['type'] == 'percentage' else val
                else:
                    response_data[keyName][key['name']] = None

    wb.close()
    return utilities.response_handler(False,None,response_data)


def handler(request, path):
    try:
        response_data = {
            'leadCode': request['leadCode']
        }
        product = request['product']

        validator(product)

        wb = openpyxl.load_workbook(path)
        sheetNameArr = constants.PRODUCT_PROGRAM_MAP[product]

        #No need to update all fields in each worksheet individually. 
        #Business team changed the requirement and have asked to update it in a common worksheet.
        
        sheet = wb[enums.ExcelSheetName.SUMMARY.value]
        print('--- WRITE_FIELDS execution STARTED ---')
        
        #loanAmount
        sheet['C14'] = request['loanAmount']
        response_data['loanAmount'] = request['loanAmount']

        #roi
        sheet['C18'] = request['roi']
        response_data['roi'] = request['roi']

        #tenure
        sheet['C17'] = request['tenure']
        response_data['tenure'] = request['tenure']
        print('--- WRITE_FIELDS execution ENDED ---\n')

        # for sheetName in sheetNameArr:
        #     print('extracting sheet by name: {}'.format(sheetName))
        #     sheet = wb[sheetName]
        #     print('--- sheet extracted ---\n')
        #     subProgram = '{}_{}'.format(product,sheetName)
        #     coordinates = constants.PROGRAM_EXCEL_CELL_MAP[subProgram]

        #     print('--- WRITE_FIELDS execution STARTED ---')
        #     for key in constants.WRITE_FIELDS:
        #         cell_coordinates = get_cell_coordinates(coordinates, key)
        #         sheet[cell_coordinates] = request[key]
        #         new_val = sheet[cell_coordinates].value
        #         response_data[key] = new_val
        #     print('--- WRITE_FIELDS execution ENDED ---\n')

        print('--- saving workbook ---')
        wb.save(path)
        print('--- workbook SAVED ---\n')
        wb.close()
        return utilities.response_handler(False,None,response_data)
    except Exception as e:
        print('Exception Raised: {}'.format(str(e)))
        return utilities.response_handler(True,str(e),None)


def update_eligibility_excel(body):
    try:
        print("in update_eligibility_excel", body)
        if(body.get('stage')=='UAT'):
            lead_code = uat_lead_code
        else:
            lead_code = body['leadCode']
        
        
        cognos_to_onedrive = msal.PublicClientApplication(CLIENT_ID, authority=AUTHORITY_URL)
        token = cognos_to_onedrive.acquire_token_by_username_password(USERNAME,PASSWORD,SCOPES)
        
        headers = {'Authorization': 'Bearer {}'.format(token['access_token'])}
        print(headers)
        
        print("..get excel from onedrive.. ")
        response_get_from_onedrive = get_from_one_drive(file_name  = lead_code, destination = 'sme_underwriting/sme_dashboard_staging/', headers=headers)
        print(" fetched --- custom log ")
        
        if response_get_from_onedrive['error']:
            return response_get_from_onedrive
        
        onedrive_source = response_get_from_onedrive['data']['source']
        
        local_file_path = f'/tmp/{lead_code}.xlsx'
        
        response = handler(body, local_file_path)
        if response['error']:
            return response
        
        response_push_to_onedrive = push_to_one_drive(file_name = lead_code, destination = onedrive_source, headers = headers, path = local_file_path)
        if response_push_to_onedrive['error']:
            return response_push_to_onedrive
        
        return response
    except Exception as e:
        print(str(e))
        return utilities.response_handler(True, str(e), None)
    
    
def fetch_eligibility_excel(body):
    try:
        print("in update_eligibility_excel", body)
        if(body.get('stage')=='UAT'):
            lead_code = uat_lead_code
        else:
            lead_code = body['leadCode']
        
        cognos_to_onedrive = msal.PublicClientApplication(CLIENT_ID, authority=AUTHORITY_URL)
        token = cognos_to_onedrive.acquire_token_by_username_password(USERNAME,PASSWORD,SCOPES)
        
        headers = {'Authorization': 'Bearer {}'.format(token['access_token'])}
        response_get_from_onedrive = get_from_one_drive(file_name  = lead_code, destination = 'sme_underwriting/sme_dashboard_staging/', headers=headers)
        
        if response_get_from_onedrive['error']:
            return response_get_from_onedrive
        
        local_file_path = f'/tmp/{lead_code}.xlsx'
        res = excel_reader(request = body, path = local_file_path, response_data = {})
        
        
        return res
    except Exception as e:
        print(str(e))
        return utilities.response_handler(True, str(e), None)