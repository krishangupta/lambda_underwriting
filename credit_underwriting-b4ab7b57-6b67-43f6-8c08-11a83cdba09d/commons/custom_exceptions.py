class DataException(Exception):
    pass


class DataNotFound(DataException):
    pass


class rtr_DNF(DataNotFound):
    pass


class gst_month_DNF(DataNotFound):
    pass


class gst_state_DNF(DataNotFound):
    pass


class gsft_score_DNF(DataNotFound):
    pass


class fs_score_DNF(DataNotFound):
    pass


class banking_DNF(DataNotFound):
    pass

