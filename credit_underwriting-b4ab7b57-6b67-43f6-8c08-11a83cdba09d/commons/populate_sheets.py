from commons.common_functions import QueryAthena
import sub_reports.data_config as data_config
import commons.db_queries as db_query

import datetime as dt
import pandas as pd
import numpy as np
import os
import boto3
import openpyxl
import json
import copy
from dateutil.relativedelta import relativedelta

# list to help with indexing of excel, gives a corresponding excel column index to a column number/index of dataframe
index_help_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ']

def query_handler(query, database, filter_clause, filter_variable, filter_required=True):
    if filter_required:
        filter_query = f"\nWHERE {filter_clause}='{filter_variable}'"
        query = query + filter_query

    athena_handler = QueryAthena(query=query, database=database)
    df = athena_handler.run_query()
    #df.round(2)
    #print(athena_handler.get_s3_file_name())
    #print(query)
    return df

def populate_status_sheet(credit_queue_df, address_df,sheet):
    print("populate_status_sheet started")
    if(len(credit_queue_df)==1):
        credit_queue_date = credit_queue_df._get_value(0,'updt_dt')
        
        sheet['C12'] = credit_queue_date
        
    print("populate_status_sheet finished")
    
    if not address_df.empty:
        sheet['C33'] = address_df.iloc[0,:].get('office_address')
        sheet['C34'] = address_df.iloc[0,:].get('residential_address')
    
    return sheet
    
def populate_additional_FS_data_points(sheet, loan_id):
    print('populate_additional_FS_data_points')
    df = df = query_handler(db_query.additional_fs_details.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
    if len(df)==0:
        print('no records found')
        return sheet

    for i, row in df.iterrows():
        col_name = data_config.additional_fs_details_col_number_mapping.get(row['fy'])
        if col_name: # colname none for fy other than 2018,2019,2020, do not enter that data
            for column_key, row_number in data_config.additional_fs_details_row_number_mapping.items():
                index = f'{col_name}{row_number}'
                sheet[index] = row[column_key]

    return sheet
    
    
    
def populate_customer_details(sheet, loan_id):
    print('populating_customer_details')
    
    
    # rag_classification = query_handler(db_query.rag_classification.replace('<lead_code>', loan_id), database='default', filter_clause='loan_no', filter_variable=loan_id, filter_required=False)
    # if(len(rag_classification)>0):
    #     rag = rag_classification.iloc[-1]
    #     sheet['C1'] = rag['classification']
    
        
    #df = query_handler(db_query.cust_details.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
    cust_details = query_handler(db_query.cust_details.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
    
    col_mapping = data_config.populate_customer_details_col_mapping
    row_mapping = data_config.populate_customer_details_row_mapping
    
    if(len(cust_details)>0):
        cust_details = cust_details.iloc[-1]
        col_number = col_mapping.get('summary_col')
        
        for column_key, row_number in row_mapping.items():
            index = f'{col_number}{row_number}'
            sheet[index] = cust_details[column_key]
    
    # cust_details = cust_details.iloc[-1]
    # sheet['C4'] = cust_details['login_date']
    # sheet['C5'] = cust_details['lead_code']
    # sheet['C6'] = cust_details['login_branch_name']
    # sheet['C7'] = cust_details['register_city']
    # sheet['C8'] = cust_details['primary_applicant_name']
    # sheet['C11'] = cust_details['industry']
    # #sheet['C12'] = cust_details['sub-industry']
    # sheet['C13'] = cust_details['product']
    # sheet['C14'] = cust_details['loan_amount_required']
    print('populated, now second set')

    df = query_handler(db_query.cust_level_scores.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
    #cust_details = query_handler(db_query.cust_level_scores.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)

    statics = data_config.populate_customer_details_static
    starting_row = statics.get('starting_row')
    
    col_mapping = data_config.populate_customer_details_applicants_col_mapping
    
    for index, row in df.iterrows():
        col = col_mapping.get('customer_id')
        print(f"col: {col}, starting_row: {starting_row}")
        sheet[f'{col}{starting_row}'] = row['customer_id']
        
        col = col_mapping.get('applicant_name')
        sheet[f'{col}{starting_row}'] = row['applicant_name']
        
        col = col_mapping.get('score')
        sheet[f'{col}{starting_row}'] = row['cibil_score']
        #sheet[f'I{starting_row}'] = row['gsft_score_decile']
        #sheet[f'L{starting_row}'] = row['gsft_score']

        starting_row += 1

    print('populate_consumer_score finished')
    df = query_handler(db_query.commercial_score.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
    #commercial_score = query_handler(db_query.commercial_score.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
    for index, row in df.iterrows():
        col = col_mapping.get('customer_id')
        sheet[f'{col}{starting_row}'] = row['customer_id']
        
        col = col_mapping.get('applicant_name')
        sheet[f'{col}{starting_row}'] = row['name']
        
        col = col_mapping.get('score')
        sheet[f'{col}{starting_row}'] = row['score_value']
        # sheet[f'E{starting_row}'] = row['customer_id']
        # sheet[f'C{starting_row}'] = row['name']
        # sheet[f'H{starting_row}'] = row['score_value']
        
    print('populate_commercial_score finished')
    
    #fetch customer ids of all applicants under a LEAD CODE/LOAN ID
    df_customer_id  = query_handler(db_query.get_all_applicants_customer_id.replace('<loan_no>', loan_id), database='los_afx_db', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
    applicants_customer_id_list = df_customer_id['customer_id'].tolist()
    
    if(len(applicants_customer_id_list) > 0):
    
        req = ""
        for i in applicants_customer_id_list:
            req = req + ",'" + str(i) + "'"
            
        applicants_customer_id_list = req[1:]  
        
        # #traverse over all customer ids & get their Bureau Consumer Score (if exists)
        df_bucn_score_decile = query_handler(db_query.bucn_score_decile.replace('<customer_no>', applicants_customer_id_list), database='gsft-analytics-db', filter_clause='customer_id', filter_variable=applicants_customer_id_list, filter_required=False)
            
        if(df_bucn_score_decile.size > 0):
            for index, row in df_bucn_score_decile.iterrows():
                starting_row = statics.get('starting_row')
                while(starting_row<=39):
                    cust_col = col_mapping.get('customer_id')
                    if(sheet[f'{cust_col}{starting_row}'].value == row['customer_id']):
                        bucn_col = col_mapping.get('bucn_score')
                        sheet[f'{bucn_col}{starting_row}'] = df_bucn_score_decile.at[0,'gsft_score_decile']
                        break
                    starting_row += 1
        
        # #traverse over all customer ids & get their Bank Statement Score (if exists)
        df_bank_score_decile = query_handler(db_query.bank_score_decile.replace('<customer_no>', applicants_customer_id_list), database='gsft-analytics-db', filter_clause='customer_id', filter_variable=applicants_customer_id_list, filter_required=False)
            
        if(df_bank_score_decile.size > 0):
            for index, row in df_bank_score_decile.iterrows():
                starting_row=statics.get('starting_row')
                while(starting_row<=39):
                    cust_col = col_mapping.get('customer_id')
                    if(sheet[f'{cust_col}{starting_row}'].value == row['customer_id']):
                        bs_col = col_mapping.get('bs_score')
                        sheet[f'{bs_col}{starting_row}'] = df_bank_score_decile.at[0,'gsft_score_decile']
                        break
                    starting_row += 1
                    
        # #traverse over all customer ids & get their bucm  Score (if exists)
        
        df_bucm_score_decile = query_handler(db_query.bucm_score_decile.replace('<customer_no>', applicants_customer_id_list), database='gsft-analytics-db', filter_clause='customer_id', filter_variable=applicants_customer_id_list, filter_required=False)
            
        if(df_bucm_score_decile.size > 0):
            for index, row in df_bucm_score_decile.iterrows():
                starting_row=statics.get('starting_row')
                while(starting_row<=39):
                    cust_col = col_mapping.get('customer_id')
                    if(sheet[f'{cust_col}{starting_row}'].value == row['customer_id']):
                        bs_col = col_mapping.get('bucm_score_decile')
                        sheet[f'{bs_col}{starting_row}'] = df_bucm_score_decile.at[0,'gsft_score_decile']
                        break
                    starting_row += 1
                    
        #     #traverse over all customer ids & get their gst score decile (if exists)
        df_gst_score_decile = query_handler(db_query.gst_score_decile.replace('<customer_no>', applicants_customer_id_list), database='gsft-analytics-db', filter_clause='customer_id', filter_variable=applicants_customer_id_list, filter_required=False)
            
        if(df_gst_score_decile.size > 0):
            for index, row in df_gst_score_decile.iterrows():
                starting_row=statics.get('starting_row')
                while(starting_row<=39):
                    cust_col = col_mapping.get('customer_id')
                    if(sheet[f'{cust_col}{starting_row}'].value == row['customer_id']):
                        bs_col = col_mapping.get('gst_score_decile')
                        sheet[f'{bs_col}{starting_row}'] = df_gst_score_decile.at[0,'score_decile']
                        break
                    starting_row += 1
                    
        #     #traverse over all customer ids & get their fs Score (if exists)
        df_fs_score_decile = query_handler(db_query.fs_score_decile.replace('<customer_no>', applicants_customer_id_list), database='gsft-analytics-db', filter_clause='customer_id', filter_variable=applicants_customer_id_list, filter_required=False)
            
        if(df_fs_score_decile.size > 0):
            for index, row in df_fs_score_decile.iterrows():
                starting_row=statics.get('starting_row')
                while(starting_row<=39):
                    cust_col = col_mapping.get('customer_id')
                    if(sheet[f'{cust_col}{starting_row}'].value == row['customer_id']):
                        bs_col = col_mapping.get('fs_score_decile')
                        sheet[f'{bs_col}{starting_row}'] = df_fs_score_decile.at[0,'gsft_score_decile']
                        break
                    starting_row += 1
    
    return sheet
    

# def rtr_total_row(rtr_df, sheet):
#     print('rtr_total_row started')
#     df_len = len(rtr_df)
#     sheet['A18'] = 'Total'
#     sheet['E18'] = f'=SUM(E20:E{df_len+19})'
#     sheet['J18'] = f'=SUM(J20:J{df_len+19})'
#     sheet['L18'] = f'=SUM(L20:L{df_len+19})'
#     sheet['M18'] = f'=SUM(M20:M{df_len+19})'
#     sheet['AI18'] = f'=SUM(AI20:AI{df_len+19})'
#     sheet['AK18'] = f'=SUM(AK20:AK{df_len+19})'
#     print('rtr_total_row finished')
#     return sheet


def rtr_sheet(rtr_df, comm_rtr_df, sheet):
    print('rtr_sheet started')
    
    row_number_total = data_config.rtr_sheet_statics.get('row_number_total')
    start_row_rtr_content = data_config.rtr_sheet_statics.get('start_row_rtr_content')
    
    #generates a list of the last 13 months (could be 14 also, but using only the last 13)
    month_ago = dt.date.today()- relativedelta(months=1)
    year_ago = month_ago - relativedelta(years=1)
    months_list = pd.date_range(year_ago, dt.date.today(), freq='MS').strftime("%b, %Y").tolist()
    # pd.to_datetime('2021-03-01') 
    
    #creating dfs corresponding to rtr_df & comm_rtr_df as per requirement
    df = pd.DataFrame(columns = ['S.No', 'Financier', 'Borrower name', 'Source', 'Ownership', 'Loan Amount', 'Account Status', 'Type', 'Loan start date', 'Year', 'POS', 'EMI Calculated', 'EMI', 'Last Reported Date', 'ROI (If available)', 'Tenor (in Months) (Actual or assumed)', 'MOB', 'Remaining tenure', 'Bounce in last 12 months', 'Status of Loan', 'Repayment Bank A/c', months_list[-1], months_list[-2], months_list[-3], months_list[-4], months_list[-5], months_list[-6], months_list[-7], months_list[-8], months_list[-9], months_list[-10], months_list[-11], months_list[-12], months_list[-13], 'No of months EMI paid in 2022-2023', 'Amount 2022-2023', 'No of months EMI paid in 2023-2024', 'Amount 2023-2024', 'Remarks'])
    df_comm = pd.DataFrame(columns = ['S.No', 'Financier', 'Borrower name', 'Source', 'Ownership', 'Loan Amount', 'Account Status', 'Type', 'Loan start date', 'Year', 'POS', 'EMI Calculated', 'EMI', 'Last Reported Date', 'ROI (If available)', 'Tenor (in Months) (Actual or assumed)', 'MOB', 'Remaining tenure', 'Bounce in last 12 months', 'Status of Loan', 'Repayment Bank A/c', months_list[-1], months_list[-2], months_list[-3], months_list[-4], months_list[-5], months_list[-6], months_list[-7], months_list[-8], months_list[-9], months_list[-10], months_list[-11], months_list[-12], months_list[-13], 'No of months EMI paid in 2022-2023', 'Amount 2022-2023', 'No of months EMI paid in 2023-2024', 'Amount 2023-2024', 'Remarks'])
    
    #processing df (rtr_df) 
    
    df['Loan Amount'] = rtr_df['disbursed_amt']
    df['Type'] = rtr_df['account_type']
    df['Loan start date'] = rtr_df['disbursed_dt']
    df['POS'] = rtr_df['current_balance']
    df['Ownership'] = rtr_df['ownership']
    df['Borrower name'] = rtr_df['applicant_name']
    df['Account Status'] = rtr_df['account_status']
    df['EMI Calculated'] = rtr_df['emi_amount_estimated']
    
    df['Tenor (in Months) (Actual or assumed)'] = rtr_df['tenure_estimated']
    
    df['Last Reported Date'] = rtr_df['date_reported']
    
    # df['ROI (If available)'] = rtr_df['rate_estimated']
    
    source_list = ['Consumer Bureau'] * len(df) #generates a list of ['Consumer Bureau', 'Consumer Bureau', ...] as long as df length
    df['Source'] = source_list
    
    df.reset_index(drop=True, inplace=True)
    # df['S.No'] = np.arange(len(df))
    
    # df_len = len(df)  # length of df
    # start_row_rtr_content = 20   # row where content starts on rtr sheet
    
    # get the index of column, then the corresponding index in EXCEL from index_help_list 
    # index_no_1 = df.columns.get_loc('Loan start date')
    # df['Year'] = df['S.No'].apply(lambda x: f'=YEAR(DATE(YEAR({index_help_list[index_no_1]}{start_row_rtr_content+x})+(MONTH({index_help_list[index_no_1]}{start_row_rtr_content+x})>3),3,31))')
    
    # index_no_1 = df.columns.get_loc('Tenor (in Months) (Actual or assumed)')
    # index_no_2 = df.columns.get_loc('EMI')
    # index_no_3 = df.columns.get_loc('Loan Amount')
    # df['ROI (If available)'] = df['S.No'].apply(lambda x: f'=RATE({index_help_list[index_no_1]}{start_row_rtr_content+x},{index_help_list[index_no_2]}{start_row_rtr_content+x},-{index_help_list[index_no_3]}{start_row_rtr_content+x},0)*12')
    
    
    # index_no_1 = df.columns.get_loc('Tenor (in Months) (Actual or assumed)')
    # index_no_2 = df.columns.get_loc('MOB')
    # df['Remaining tenure'] = df['S.No'].apply(lambda x: f'={index_help_list[index_no_1]}{start_row_rtr_content+x}-{index_help_list[index_no_2]}{start_row_rtr_content+x}')
    
    # index_no_1 = df.columns.get_loc('No of months EMI paid in 2020-2021')
    # index_no_2 = df.columns.get_loc('EMI')
    # df['Amount 2020-2021'] = df['S.No'].apply(lambda x: f'={index_help_list[index_no_1]}{start_row_rtr_content+x}*{index_help_list[index_no_2]}{start_row_rtr_content+x}')
    
    # index_no_1 = df.columns.get_loc('No of months EMI paid in 2021-2022')
    # index_no_2 = df.columns.get_loc('EMI')
    # df['Amount 2021-2022'] = df['S.No'].apply(lambda x: f'={index_help_list[index_no_1]}{start_row_rtr_content+x}*{index_help_list[index_no_2]}{start_row_rtr_content+x}')
    
    # df['S.No'] = df['S.No'] + 1
    
    # print("here")
    # print(list(zip(df['Account Status'],rtr_df['account_status'])))
    
    # obligate_mapping = {'Active':1,
    #                     'Closed':0}
    # df['Obligate'] = df['Account Status'].map(obligate_mapping)  #mapping values in Obligate column with 1 or 0 w.r.t. value in the corresponding Account Status column
    
    
    df_len = len(df)
    
    #row number to display the TOTAL row in rtr sheet
    #row_number_total = 17

    # processing comm_rtr df now
    
    df_comm['Borrower name'] = comm_rtr_df['name']
    df_comm['Loan Amount'] = comm_rtr_df['sanctioned_amount']
    df_comm['Account Status'] = comm_rtr_df['credit_facility_status']
    df_comm['Type'] = comm_rtr_df['credit_facility_type']
    df_comm['Loan start date'] = comm_rtr_df['sanction_date']
    df_comm['POS'] = comm_rtr_df['current_balance'] 
    
    source_list = ['Commercial Bureau'] * len(comm_rtr_df) #generates a list of ['Commercial Bureau', 'Commercial Bureau', ...] as long as df length
    df_comm['Source'] = source_list
    
    df_comm.reset_index(drop=True, inplace=True)
    df_comm['S.No'] = np.arange(len(comm_rtr_df))
    
    df_comm['Last Reported Date'] = comm_rtr_df['last_reported_date']
    
    df_comm_len = len(df_comm)  # length of comm_rtr_df
    df_rtr_len = len(df)  # length of rtr_df
    #start_row_rtr_content = 20   # row where content starts on rtr sheet
    
    # index_no_1 = df_comm.columns.get_loc('Loan start date')
    # df_comm['Year'] = df_comm['S.No'].apply(lambda x: f'=YEAR(DATE(YEAR({index_help_list[index_no_1]}{start_row_rtr_content+x+df_rtr_len})+(MONTH({index_help_list[index_no_1]}{start_row_rtr_content+x+df_rtr_len})>3),3,31))')
   
    #df_comm['S.No'] = df_comm['S.No'] + 1 + df_rtr_len
    
    account_status_mapping = {
        'ACTIVE':'Active',
        'CLOSED':'Closed'
    }
    df_comm['Account Status'] = df_comm['Account Status'].map(account_status_mapping) #mapping values in Account Status column w.r.t. the values in the corresponding Account Status(same dataframe) column
    
    #df_comm['Obligate'] = df_comm['Account Status'].map(obligate_mapping) #mapping values in Obligate column with 1 or 0 w.r.t. value in the corresponding Account Status column
    
    #concatenating df & comm_df
    result = pd.concat([df, df_comm], ignore_index=True, sort=False)
    result.sort_values(by = ['Account Status', 'Source'], inplace = True, ascending=True)
    result.reset_index(drop = True, inplace=True)
    result.drop(['S.No'], axis=1, inplace=True)
    
    result_len = len(result)
    more_rows = data_config.rtr_sheet_statics.get('more_rows')
    result_helper = pd.DataFrame(np.arange(result_len+more_rows))
    result_helper.columns = ['S.No']
    
    result=pd.concat([result_helper, result], axis=1)
    
    #arr = np.arange(result_len+100)
    #result['S.No'] = pd.Series(arr)
    
    
    #start_row_rtr_content = 20   # row where content starts on rtr sheet
    
    #get the index of column in dataframe, then the corresponding index in EXCEL from index_help_list 
    index_no_1 = result.columns.get_loc('Loan start date')
    result['Year'] = result['S.No'].apply(lambda x: f'=YEAR(DATE(YEAR({index_help_list[index_no_1]}{start_row_rtr_content+x})+(MONTH({index_help_list[index_no_1]}{start_row_rtr_content+x})>3),3,31))')
    
    index_no_1 = result.columns.get_loc('Tenor (in Months) (Actual or assumed)')
    index_no_2 = result.columns.get_loc('EMI')
    index_no_3 = result.columns.get_loc('Loan Amount')
    result['ROI (If available)'] = result['S.No'].apply(lambda x: f'=RATE({index_help_list[index_no_1]}{start_row_rtr_content+x},{index_help_list[index_no_2]}{start_row_rtr_content+x},-{index_help_list[index_no_3]}{start_row_rtr_content+x},0)*12')
    
    
    index_no_1 = result.columns.get_loc('Tenor (in Months) (Actual or assumed)')
    index_no_2 = result.columns.get_loc('MOB')
    result['Remaining tenure'] = result['S.No'].apply(lambda x: f'={index_help_list[index_no_1]}{start_row_rtr_content+x}-{index_help_list[index_no_2]}{start_row_rtr_content+x}')
    
    index_no_1 = result.columns.get_loc('No of months EMI paid in 2022-2023')
    index_no_2 = result.columns.get_loc('EMI')
    result['Amount 2022-2023'] = result['S.No'].apply(lambda x: f'={index_help_list[index_no_1]}{start_row_rtr_content+x}*{index_help_list[index_no_2]}{start_row_rtr_content+x}')
    
    index_no_1 = result.columns.get_loc('No of months EMI paid in 2023-2024')
    index_no_2 = result.columns.get_loc('EMI')
    result['Amount 2023-2024'] = result['S.No'].apply(lambda x: f'={index_help_list[index_no_1]}{start_row_rtr_content+x}*{index_help_list[index_no_2]}{start_row_rtr_content+x}')
    
    result['S.No'] = result['S.No'] + 1
    
    
    #row number to display the TOTAL row in rtr sheet
    #row_number_total = 17
    
    #get the index of column in dataframe, then the corresponding index in EXCEL from index_help_list 
    index_no = result.columns.get_loc('S.No')
    sheet[f'{index_help_list[index_no]}{row_number_total}'] = "TOTAL"
    
    index_no = result.columns.get_loc('Loan Amount')
    sheet[f'{index_help_list[index_no]}{row_number_total}'] = f'=SUM({index_help_list[index_no]}{start_row_rtr_content}:{index_help_list[index_no]}{result_len+start_row_rtr_content-1+more_rows})'
    
    index_no = result.columns.get_loc('POS')
    sheet[f'{index_help_list[index_no]}{row_number_total}'] = f'=SUM({index_help_list[index_no]}{start_row_rtr_content}:{index_help_list[index_no]}{result_len+start_row_rtr_content-1+more_rows})'
    
    index_no = result.columns.get_loc('EMI')
    sheet[f'{index_help_list[index_no]}{row_number_total}'] = f'=SUM({index_help_list[index_no]}{start_row_rtr_content}:{index_help_list[index_no]}{result_len+start_row_rtr_content-1+more_rows})'
    
    # index_no = result.columns.get_loc('Obligate')
    # sheet[f'{index_help_list[index_no]}{row_number_total}'] = f'=SUM({index_help_list[index_no]}{start_row_rtr_content}:{index_help_list[index_no]}{result_len+start_row_rtr_content-1+more_rows})'
    
    index_no = result.columns.get_loc('Amount 2022-2023')
    sheet[f'{index_help_list[index_no]}{row_number_total}'] = f'=SUM({index_help_list[index_no]}{start_row_rtr_content}:{index_help_list[index_no]}{result_len+start_row_rtr_content-1+more_rows})'
    
    index_no = result.columns.get_loc('Amount 2023-2024')
    sheet[f'{index_help_list[index_no]}{row_number_total}'] = f'=SUM({index_help_list[index_no]}{start_row_rtr_content}:{index_help_list[index_no]}{result_len+start_row_rtr_content-1+more_rows})'
    
    print('rtr_sheet finished')
    return result
    
    
def gsft_score_and_bank_score_df_fun(gsft_score_df, bank_score_df):
    print('gsft_score_and_bank_score_df_fun satrted')
    
    gsft_score_df.drop(['partner_name'], axis=1, inplace = True)
    
    model_name_list = ['BUCN'] * len(gsft_score_df) # generates a list of ['BUCN', 'BUCN', ...] as long as df length
    #df['Source'] = source_list
    model_name_col = np.asarray(model_name_list)
    gsft_score_df['Model Name'] = model_name_col
    
    
    model_name_list = ['BS'] * len(bank_score_df) # generates a list of ['BS', 'BS', ...] as long as df length
    model_name_col = np.asarray(model_name_list)
    bank_score_df['Model Name'] = model_name_col
    
    #concatenates two dfs 
    result = pd.concat([gsft_score_df, bank_score_df], ignore_index=True, sort=False)
    return result
    
def gtp_lap_bl(fs_score_df, sheet):
    
    # fs_score_df.set_index('', inplace=True) 
    
    print('gtp_lap_bl started')
    print(fs_score_df)

    Y1 = 'FY2020'
    Y2 = 'FY2021'
    Y3 = 'FY2022'
    
    
    gtp_calc = {}
    try:
        gtp_calc['profitbeforetax_Y1'] = fs_score_df[Y1].loc['profitbeforetax']
    except Exception as e:
        print("for y1", str(e))
        pass
    try:
        gtp_calc['profitbeforetax_Y2'] = fs_score_df[Y2].loc['profitbeforetax']
    except:
        pass
    try:
        gtp_calc['profitbeforetax_Y3'] = fs_score_df[Y3].loc['profitbeforetax']
    except:
        pass
    try:
        gtp_calc['dep_amo_Y1'] = fs_score_df[Y1].loc['dep_amo']
    except Exception as e:
        print("for Y1",str(e))
        pass
    try:
        gtp_calc['dep_amo_Y2'] = fs_score_df[Y2].loc['dep_amo']
    except:
        pass
    try:
        gtp_calc['dep_amo_Y3'] = fs_score_df[Y3].loc['dep_amo']
    except:
        pass
    try:
        gtp_calc['ExpsFinanceCostTotal_Y1'] = fs_score_df[Y1].loc['ExpsFinanceCostTotal']
    except:
        pass
    try:
        gtp_calc['ExpsFinanceCostTotal_Y2'] = fs_score_df[Y2].loc['ExpsFinanceCostTotal']
    except:
        pass
    try:
        gtp_calc['ExpsFinanceCostTotal_Y3'] = fs_score_df[Y3].loc['ExpsFinanceCostTotal']
    except:
        pass
    print('gtp_lap_bl finished')
    return set_gtp_values(gtp_calc, sheet)


def set_gtp_values(gtp_calc, sheet):
    col_mapping = data_config.set_gtp_values_col_mapping
    row_mapping = data_config.set_gtp_values_row_mapping
    
    print("set_gtp_values_started")
    print("gtp_calc",gtp_calc)
    
    # for i, row in df.iterrows():
    #     col_name = data_config.additional_fs_details_col_number_mapping.get(row['fy'])
    #     if col_name: # colname none for fy other than 2018,2019,2020, do not enter that data
    #         for column_key, row_number in data_config.additional_fs_details_row_number_mapping.items():
    #             index = f'{col_name}{row_number}'
    #             sheet[index] = row[column_key]
    
    row = row_mapping.get('profitbeforetax')
    
    try:
        col = col_mapping.get('Y1')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('profitbeforetax_Y1')
    except:
        pass
    
    try:
        col = col_mapping.get('Y2')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('profitbeforetax_Y2')
    except:
        pass
    
    try:
        col = col_mapping.get('Y3')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('profitbeforetax_Y3')
    except:
        pass

    row = row_mapping.get('dep_amo')
    
    try:
        col = col_mapping.get('Y1')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('dep_amo_Y1')
    except:
        pass
    
    try:
        col = col_mapping.get('Y2')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('dep_amo_Y2')
    except:
        pass
    
    try:
        col = col_mapping.get('Y3')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('dep_amo_Y3')
    except:
        pass
    
    row = row_mapping.get('exps_finance_cost')
    
    try:
        col = col_mapping.get('Y1')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('ExpsFinanceCostTotal_Y1')
    except:
        pass
    
    try:
        col = col_mapping.get('Y2')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('ExpsFinanceCostTotal_Y2')
    except:
        pass
    
    try:
        col = col_mapping.get('Y3')
        index = f'{col}{row}'
        sheet[index] = gtp_calc.get('ExpsFinanceCostTotal_Y3')
    except:
        pass
    
    # sheet['C8'] = gtp_calc.get('profitbeforetax_2018')
    # sheet['D8'] = gtp_calc.get('profitbeforetax_2019')
    # sheet['E8'] = gtp_calc.get('profitbeforetax_2020')

    # sheet['C12'] = gtp_calc.get('dep_amo_2018')
    # sheet['D12'] = gtp_calc.get('dep_amo_2019')
    # sheet['E12'] = gtp_calc.get('dep_amo_2020')

    # sheet['C13'] = gtp_calc.get('ExpsFinanceCostTotal_2018')
    # sheet['D13'] = gtp_calc.get('ExpsFinanceCostTotal_2019')
    # sheet['E13'] = gtp_calc.get('ExpsFinanceCostTotal_2020')

    print('set_gtp_values_started, gtp_lap_bl sheet done')
    return sheet


def revenue_estimation(gst_month_df, fs_score_df, sheet):
    year_map = {
        'Y1': '2021',
        'Y2': '2022',
        'Y3': '2023'
    }
    year_map_fy = {
        'Y1': 'FY2021',
        'Y2': 'FY2022',
        'Y3': 'FY2023'
    }
    print('revenue_estimation started')
    
    gst_month_df = gst_month_df[gst_month_df['category']=='sales_gstr3b'][['ret_period', 'ttl_tax']]
    gst_month_df = gst_month_df.groupby(['ret_period']).max()
    gst_month_df.columns = ['ttl_tax']
    gst_month_df.reset_index(inplace=True)
    gst_month_df['ret_period'] = gst_month_df['ret_period'].apply(pd.to_datetime).apply(lambda x: x.strftime('%Y-%m-%d'))
    print(gst_month_df)
    gst_month_df.set_index(['ret_period'], drop=True, inplace=True)
    
    revenue_calc = json.loads(gst_month_df.to_json(orient='index'))
    
    for key in list(revenue_calc):
        if type(revenue_calc[key])==dict:
            revenue_calc[key] = revenue_calc[key].get('ttl_tax', None)
            #print(f'revenue_calc: {key}:{revenue_calc[key]}, {type(revenue_calc[key])}')
            if ((revenue_calc[key] is not None) and (type(revenue_calc[key]) not in [int, float])):
                print(f'type issue: {key}:{revenue_calc[key]}, {type(revenue_calc[key])}')
                del revenue_calc[key]
    
    try:    
        revenue_calc[f'audited_sales_{year_map["Y1"]}'] = fs_score_df[year_map_fy["Y1"]][20]
    except:
        pass
    try:
        revenue_calc[f'ebitda_{year_map["Y1"]}'] = fs_score_df[year_map_fy["Y1"]][21]
    except:
        pass
    try:
        revenue_calc[f'audited_sales_{year_map["Y2"]}'] = fs_score_df[year_map_fy["Y2"]][20]
    except:
        pass
    try:
        revenue_calc[f'ebitda_{year_map["Y2"]}'] = fs_score_df[year_map_fy["Y2"]][21]
    except:
        pass
    try:
        revenue_calc[f'audited_sales_{year_map["Y3"]}'] = fs_score_df[year_map_fy["Y2"]][20]
    except:
        pass

    print('revenue_estimation finished')
    return set_revenue_values(sheet, revenue_calc, year_map)
    
    
def set_revenue_values(sheet, revenue_calc, year_map):
    print("set_gtp_values_started")
    
    col_mapping = data_config.set_revenue_value_mom_revenue_trend_col_mapping
    row_mapping = data_config.set_revenue_value_mom_revenue_trend_row_mapping
    
    
    row = row_mapping.get('audited_sales')
    
    col = col_mapping.get('Y1')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'audited_sales_{year_map["Y1"]}',0)
    
    col = col_mapping.get('Y2')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'audited_sales_{year_map["Y2"]}',0)
    
    col = col_mapping.get('Y3')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'audited_sales_{year_map["Y3"]}',0)
    
    
    # sheet['C7'] = revenue_calc.get('audited_sales_2018', 0)
    # sheet['D7'] = revenue_calc.get('audited_sales_2019', 0)
    # sheet['E7'] = revenue_calc.get('audited_sales_2020', '=C29')
    #sheet['C43'] = revenue_calc.get('ebitda_2018', 0)
    #sheet['D43'] = revenue_calc.get('ebitda_2019', 0)
    
    # col = col_mapping.get('FY2019/20')
    # for column_key, row_number in col_mapping.items():
    #     row = row_mapping.get(row_number)
    #     index = f'{col}{row}'
    #     sheet[index] = revenue_calc.get(column_key)
    
    ##MOM Revenue Trend, FY2019/20
    
    col = col_mapping.get('FY1/FY2')
    
    row = row_mapping.get('April')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-04-01')
    
    row = row_mapping.get('May')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-05-01')
    
    row = row_mapping.get('June')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-06-01')
    
    row = row_mapping.get('July')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-07-01')
    
    row = row_mapping.get('August')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-08-01')
    
    row = row_mapping.get('September')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-09-01')
    
    row = row_mapping.get('October')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-10-01')
    
    row = row_mapping.get('November')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-11-01')
    
    row = row_mapping.get('December')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y1"]}-12-01')
    
    row = row_mapping.get('January')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-01-01')
    
    row = row_mapping.get('February')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-02-01')
    
    row = row_mapping.get('March')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-03-01')
    
    
    col = col_mapping.get('FY2/FY3')
    
    row = row_mapping.get('April')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-04-01','')
    
    row = row_mapping.get('May')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-05-01','')
    
    row = row_mapping.get('June')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-06-01','')
    
    row = row_mapping.get('July')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-07-01','')
    
    row = row_mapping.get('August')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-08-01','')
    
    row = row_mapping.get('September')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-09-01','')
    
    row = row_mapping.get('October')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-10-01','')
    
    row = row_mapping.get('November')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-11-01','')
    
    row = row_mapping.get('December')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y2"]}-12-01','')
    
    row = row_mapping.get('January')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-01-01','')
    
    row = row_mapping.get('February')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-02-01','')
    
    row = row_mapping.get('March')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-03-01','')
    
    ## MOM Revenue Trend, estimate2020/21
    
    
    
    col = col_mapping.get('estimate2020/21')
    
    row = row_mapping.get('April')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-04-01','=D17*(1+D33)')
    
    row = row_mapping.get('May')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-05-01','=D18*(1+D33)')
    
    row = row_mapping.get('June')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-06-01','=D19*(1+D33)')
    
    row = row_mapping.get('July')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-07-01','=D20*(1+D33)')
    
    row = row_mapping.get('August')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-08-01','=D21*(1+D33)')
    
    row = row_mapping.get('September')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-09-01','=D22*(1+D33)')
    
    row = row_mapping.get('October')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-10-01','=D23*(1+D33)')
    
    row = row_mapping.get('November')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-11-01','=D24*(1+D33)')
    
    row = row_mapping.get('December')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get(f'{year_map["Y3"]}-12-01','=D25*(1+D33)')
    
    row = row_mapping.get('January')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get('2023-01-01','=D26*(1+D33)')
    
    row = row_mapping.get('February')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get('2023-02-01','=D27*(1+D33)')
    
    row = row_mapping.get('March')
    index = f'{col}{row}'
    sheet[index] = revenue_calc.get('2023-03-01','=D28*(1+D33)')
    
    
    
    
    # sheet['C17'] = revenue_calc.get('2019-04-01')
    # sheet['C18'] = revenue_calc.get('2019-05-01')
    # sheet['C19'] = revenue_calc.get('2019-06-01')
    # sheet['C20'] = revenue_calc.get('2019-07-01')
    # sheet['C21'] = revenue_calc.get('2019-08-01')
    # sheet['C22'] = revenue_calc.get('2019-09-01')
    # sheet['C23'] = revenue_calc.get('2019-10-01')
    # sheet['C24'] = revenue_calc.get('2019-11-01')
    # sheet['C25'] = revenue_calc.get('2019-12-01')
    # sheet['C26'] = revenue_calc.get('2020-01-01')
    # sheet['C27'] = revenue_calc.get('2020-02-01')
    # sheet['C28'] = revenue_calc.get('2020-03-01')
    
    # sheet['D17'] = revenue_calc.get('2020-04-01', '=C17*(1+C33)')
    # sheet['D18'] = revenue_calc.get('2020-05-01', '=C18*(1+C33)')
    # sheet['D19'] = revenue_calc.get('2020-06-01', '=C19*(1+C33)')
    # sheet['D20'] = revenue_calc.get('2020-07-01', '=C20*(1+C33)')
    # sheet['D21'] = revenue_calc.get('2020-08-01', '=C21*(1+C33)')
    # sheet['D22'] = revenue_calc.get('2020-09-01', '=C22*(1+C33)')
    # sheet['D23'] = revenue_calc.get('2020-10-01', '=C23*(1+C33)')
    # sheet['D24'] = revenue_calc.get('2020-11-01', '=C24*(1+C33)')
    # sheet['D25'] = revenue_calc.get('2020-12-01', '=C25*(1+C33)')
    # sheet['D26'] = revenue_calc.get('2021-01-01', '=C26*(1+C33)')
    # sheet['D27'] = revenue_calc.get('2021-02-01', '=C27*(1+C33)')
    # sheet['D28'] = revenue_calc.get('2021-03-01', '=C28*(1+C33)')

    print('revenue_estimation sheet done')
    return sheet


def validate_revenue_estimation_output(revenue_json):
    print(revenue_json)
    status = "success"
    reason = ""
    if (revenue_json.get('gst_month_df', 0)==0):
        reason += "len(gst_month_df) = 0; "
        status = 'fail'
        
    if (revenue_json.get('gst_state_df', 0)==0):
        reason += "len(gst_state_df) = 0; "
        status = 'fail'

    if (revenue_json.get('gsft_score_df', 0)==0):
        reason += "len(gsft_score_df) = 0; "
        status = 'fail'
        
    if (revenue_json.get('fs_score_df', 0)==0):
        reason += "len(fs_score_df) = 0; "
        status = 'fail'
    
    return {
        'status':status,
        'reason':reason
    }


def school_data(sheet,school_df):
    
    row_mapping = data_config.school_data_row_mapping
    col_mapping = data_config.school_data_col_mapping
    
    classes = list(data_config.school_db_field_map.keys())
    
    ym_fee = school_df.get('yearly_monthly')
    tuition_multiplier = 12 if ym_fee=='M' else 1
    for this_class in classes:
        row = row_mapping.get(this_class)
        student_col = col_mapping['students_y1']
        tution_col = col_mapping['tuition_y1']
        other_fee_col = col_mapping['ann_charges_y1']
        
        sheet[f'{student_col}{row}'] = school_df.get(data_config.school_db_field_map[this_class]['students'])
        sheet[f'{tution_col}{row}'] = school_df.get(data_config.school_db_field_map[this_class]['tuition_fee']) * tuition_multiplier
        sheet[f'{other_fee_col}{row}'] = school_df.get(data_config.school_db_field_map[this_class]['yearly_development_charges']) + school_df.get(data_config.school_db_field_map[this_class]['other_charges'])
        
    return sheet