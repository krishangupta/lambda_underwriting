from enum import Enum

class FieldDetail:
    def __init__(self, key, cellDetail, formula) -> None:
        self.key = key
        self.cellDetail = cellDetail
        self.formula = formula

class CellDetail:
    def __init__(self, coordinates, sheetName) -> None:
        self.coordinates = coordinates
        self.sheetName = sheetName

class ExcelSheetName(Enum):
    FIN_SUMMARY = "FS"
    RTR = "RTR"
    GTP_LAP_BL = "GTP_LAP_BL"
    ASSESSED_LAP = "Assessed_LAP"
    BANKING_SURROGATE = "Banking Surrogate"

SCREEN_CELL_MAP = {
    'financialDetails' : [
        FieldDetail(**{
            "key": 'latestYearTurnover', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'E66', "sheetName": ExcelSheetName.FIN_SUMMARY.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'previousYearTurnover', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'F66', "sheetName": ExcelSheetName.FIN_SUMMARY.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'latestYearCashProfit', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'E18', "sheetName": ExcelSheetName.GTP_LAP_BL.value}),
                CellDetail(**{"coordinates": 'C49', "sheetName": ExcelSheetName.ASSESSED_LAP.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'ebidta', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'F20', "sheetName": ExcelSheetName.GTP_LAP_BL.value}),
                CellDetail(**{"coordinates": 'D14', "sheetName": ExcelSheetName.ASSESSED_LAP.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'annualizedCredit', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'F5', "sheetName": ExcelSheetName.BANKING_SURROGATE.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'abb', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'F3', "sheetName": ExcelSheetName.BANKING_SURROGATE.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'stockDays', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'E147', "sheetName": ExcelSheetName.FIN_SUMMARY.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'debtorDays', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'E148', "sheetName": ExcelSheetName.FIN_SUMMARY.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'creditorDays', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'E149', "sheetName": ExcelSheetName.FIN_SUMMARY.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'abbEmi', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'I3', "sheetName": ExcelSheetName.BANKING_SURROGATE.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'dscr', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'E158', "sheetName": ExcelSheetName.FIN_SUMMARY.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'bto', 
            "cellDetail": None,
            "formula": 'BTO'
        }),
    ],
    'obligationSummary' : [
        FieldDetail(**{
            "key": 'posAllLoans', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'K18', "sheetName": ExcelSheetName.RTR.value})
            ],
            "formula": None
        }),
        FieldDetail(**{
            "key": 'totalMonthlyObligations', 
            "cellDetail": [
                CellDetail(**{"coordinates": 'AJ18', "sheetName": ExcelSheetName.RTR.value})
            ],
            "formula": 'TOTAL_OBLIGATIONS'
        }),
        FieldDetail(**{
            "key": 'posAllLoansTurnover', 
            "cellDetail": None,
            "formula": 'POS_TURNOVER'
        })
    ]
}