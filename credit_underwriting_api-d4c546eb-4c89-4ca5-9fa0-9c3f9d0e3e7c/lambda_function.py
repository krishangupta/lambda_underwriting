import json
from endpoint.get_report import get_report
from endpoint.get_s3_file_download_link import get_s3_file_download_link
from endpoint.get_loan_list import get_loan_list
from endpoint.update_eligibility_excel import update_eligibility_excel
from endpoint.fetch_eligibility_excel import fetch_eligibility_excel

from endpoint.fetch_cam_summary_data import fetch_cam_summary_data

import datetime as dt

def lambda_handler(event, context):
    print("lambda triggered at ", dt.datetime.now())
    print(json.dumps(event))
    resource = event.get('resource')
    body = []
    if(event.get('body')!=None and event.get('body')!=""):
            body = json.loads(event.get('body'))
    
    response = {}
    if(resource=='/get-report'):
        response = get_report(body)
    elif(resource=='/get-s3-file-download-link'):
        response = get_s3_file_download_link(body)
    elif(resource=='/get-loan-list'):
        response = get_loan_list(body)
    elif(resource == '/update-eligibility-excel'):
        response = update_eligibility_excel(body)
        #response = "update-eligibility-excel"
    elif(resource == '/fetch-eligibility-excel'):
        response = fetch_eligibility_excel(body)
    elif(resource == '/fetch-cam-summary-data'):
        response = fetch_cam_summary_data(body)
    else:
        response = "INVALID ENDPOINT"
        
    print("response in lambda: ")
    print(response)
    
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': json.dumps(response)
    }
