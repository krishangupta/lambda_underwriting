from commons.common_functions import QueryAthena, HelperClassS3, copy_cell_value_openpyxl, copy_df_to_excel_with_formatting
from commons.push_to_onedrive import push_files_to_onedrive
from commons.formulas_writing import case_details_formulas, fs_formulas, revenue_estimation_formulas, gtp_lap_bl_formulas, rtr_formulas
from commons.populate_sheets import query_handler, populate_status_sheet, populate_additional_FS_data_points, populate_customer_details, rtr_sheet
from dateutil.relativedelta import relativedelta



import sub_reports.data_config as data_config
import commons.custom_exceptions as custom_exceptions
import commons.db_queries as db_query
import datetime as dt
import pandas as pd
import numpy as np
import os
import boto3
import openpyxl
import json
import copy




def generate_report(loan_id):
    print("in generate_report")
    try:
        local_template_path = '/tmp/eil_template_v3.xlsx'
        print(local_template_path)
        
        rtr_df = query_handler(db_query.loan_level_rtr_query.replace('<loan_no>', loan_id), database='default', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        print("our custom log---> ", rtr_df)
        print("our custom log---> ", rtr_df.info())
        rtr_df.replace(np.nan, '', regex=True, inplace=True)
        rtr_df.sort_values(by=['account_status' , 'disbursed_amt'] , inplace=True , ascending=True)
        ### change data type from datetime to date -- rtr_df
        rtr_df['disbursed_dt'] = pd.to_datetime(rtr_df['disbursed_dt']).dt.date
        rtr_df['date_reported'] = pd.to_datetime(rtr_df['date_reported']).dt.date

        comm_rtr_df = query_handler(db_query.loan_level_comm_rtr_query.replace('<loan_no>', loan_id), database = 'tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
        comm_rtr_df.replace(np.nan, '', regex=True, inplace=True)
        ### change data type from datetime to date
        comm_rtr_df['sanction_date'] = pd.to_datetime(comm_rtr_df['sanction_date']).dt.date
        comm_rtr_df['last_reported_date'] = pd.to_datetime(comm_rtr_df['last_reported_date']).dt.date
        
        # fs_score_df = query_handler(db_query.loan_level_fs_data_query, database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id)
        # fs_score_df.replace(np.nan, '', regex=True, inplace=True)
        # fs_score_df.sort_values(by=['fy'], inplace=True, ascending=True)

        # # fs Transpose
        # fs_score_df = fs_score_df.T
        # fs_score_df_columns = []
        # for i in range(0, len(fs_score_df.columns)):
        #     fs_score_df_columns.append(fs_score_df[i]['fy'])
    
        # fs_score_df.columns = fs_score_df_columns
        # fs_score_df.drop(['fy'], inplace=True)
        # fs_score_df.reset_index(inplace=True)
        # fs_score_df.rename(columns={'index':''}, inplace=True)
        
        workbook = openpyxl.load_workbook(local_template_path)
        #print(workbook.sheetnames)
        
        excel_writer = pd.ExcelWriter(local_template_path, engine='openpyxl')
        excel_writer.book = workbook
        excel_writer.sheets = dict((ws.title, ws) for ws in workbook.worksheets)
        
        excel_writer.sheets['RTR_MASTER'] = workbook.create_sheet('RTR_MASTER')
        excel_writer.sheets['COMM_RTR_MASTER'] = workbook.create_sheet('COMM_RTR_MASTER')
        
        copy_df_to_excel_with_formatting(rtr_df, excel_writer.sheets['RTR_MASTER'], start_row=0)
        copy_df_to_excel_with_formatting(comm_rtr_df, excel_writer.sheets['COMM_RTR_MASTER'], start_row=0)
        
        rtr_template_df = rtr_sheet(rtr_df,comm_rtr_df, excel_writer.sheets['RTR'])
        copy_df_to_excel_with_formatting(rtr_template_df, excel_writer.sheets['RTR'], start_row=18, auto_filter_row='RTR')
        
        populate_customer_details(excel_writer.sheets['SUMMARY'], loan_id)
        populate_additional_FS_data_points(excel_writer.sheets['FS'], loan_id)
        
        fs_formulas(excel_writer.sheets['FS'])
        
        excel_writer.save()
        
        return {
            'local_file_name':local_template_path,
            # 'gst_month_df':len(gst_month_df),
            # 'gst_state_df':len(gst_state_df),
            # 'gsft_score_df':len(gsft_score_df),
            # 'fs_score_df':len(fs_score_df)
        }
        

    except custom_exceptions.rtr_DNF as ex:
        return {"status":"fail", "loan_id":f"{loan_id}"}


def eil_report(loan_id, body=None):
    print("in run_report")
    bucket_name = 'datalake-gsft'
    s3_helper = HelperClassS3()
    s3_helper.copy_s3_file_to_local('/tmp/', 'eil_template_v3.xlsx', bucket_name, 'sme_underwriting/')

    return_json = generate_report(loan_id)
    print("generate_report called")
    
    if return_json.get('status', None)=='fail':
        return return_json
    
    local_file_name = return_json['local_file_name']
    local_file_path = '/'.join(local_file_name.split('/')[:-1])+'/'
    local_file_name = local_file_name.split('/')[-1]
    s3_file_name = f'{loan_id}.xlsx'
    
    os.rename(local_file_path+local_file_name, local_file_path+s3_file_name)
    print("os rename called")
    return_json['local_file_name'] = local_file_path+s3_file_name
    todays_date = str(dt.datetime.today().date())
    
    print("s3 push init")
    return_json['s3_file_name'] = 's3://' + bucket_name + '/' + s3_helper.copy_local_file_to_s3(local_file_path, s3_file_name, bucket_name, f'sme_pre_online_dashboard/reports/{todays_date}/')
    print("s3 push completed")
    #return_json.update(validate_revenue_estimation_output(return_json))
    #print("validate_revenue_estimation_output json updated")
    push_files_to_onedrive(return_json['local_file_name'], body)
    
    return return_json