import datetime as dt
import json

from sub_reports.loan_level_revenue_estimation import run_report
from sub_reports.eil_revenue_estimation import eil_report
from read_reports.main import fetch_cam_summary_data
from commons.common_functions import HelperClassS3, log_fails
from update_reports.main import update_eligibility_excel, fetch_eligibility_excel
from commons.constants import uat_cam_url
from commons.utils import meraki_cam_callback

import traceback

def lambda_handler(event, context):
    
    
    
    
    print(json.dumps(event))
    cam_type = event.get('cam_type', '')
    x1 = dt.datetime.now()
    
    loan_id = event.get('loan_id', 'BL-002656')
    cbse_aff_id = event.get('cbse_aff_id', None)
    
    if(event.get('stage')=='UAT' and cam_type in ('EIL', 'SME')):
        try:
            outjson = meraki_cam_callback(event, uat_cam_url, False, 'cam url generated successfully')
        except Exception as e:
            outjson = {'error':True, 'errorMsg': str(e), 'data': None}
            
    elif(cam_type == 'update-eligibility-excel'):
        outjson = update_eligibility_excel(event)
        
    elif(cam_type == 'fetch-eligibility-excel'):
        outjson = fetch_eligibility_excel(event)
        
    elif(cam_type == 'fetch-cam-summary-data'):
        outjson = fetch_cam_summary_data(event)
    
    elif(cam_type=='EIL'):
        print("in if EIL ---")
        try:
            outjson = eil_report(loan_id, event)
            if outjson.get('status', None)=='fail':
                log_fails(outjson, loan_id)
        except Exception as ex:
            print(traceback.format_exc())
            outjson = {'status':'fail', 'reason': ex}
            meraki_cam_callback(event, '', True, str(ex))
        
        print("--EIL EXECUTED---")
    
    # #LAP-000804, BL-001111
    else:
        print("in else, i.e. SME (normal template) ---")
        try:
            outjson = run_report(loan_id,cbse_aff_id, event)
            if outjson.get('status', None)=='fail':
                log_fails(outjson, loan_id)
    
        except Exception as ex:
            print(traceback.format_exc())
            outjson = {'status':'fail', 'reason': ex}
            meraki_cam_callback(event, '', True, str(ex))
    
    print(f'returning outjson: {outjson}\ntype:{type(outjson)}')
    print(f'time taken: {dt.datetime.now()-x1}')
    
    return outjson
    
    #return "HELLO, LAMBDA EXECUTED!!!"
