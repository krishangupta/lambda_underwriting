import boto3
from botocore.exceptions import ClientError
import logging

# def get_s3_file_download_link(body):
#     BUCKET_NAME = "datalake-gsft"
#     #KEY = body.get("file_name") 
#     KEY = 'sme_underwriting/reports/2021-01-22/BL-000574.xlsx'
#     print(KEY)

#     s3 = boto3.resource('s3')

#     try:
#         s3.Bucket(BUCKET_NAME).download_file(KEY, '/tmp/report.xlsx')
#     except botocore.exceptions.ClientError as e:
#         if e.response['Error']['Code'] == "404":
#             print("The file does not exist.")
#         else:
#             raise
        

def get_s3_file_download_link(body, expiration=3600):
    BUCKET_NAME = "datalake-gsft"
    #KEY = body.get("file_name") 
    KEY = 'sme_underwriting/reports/2021-01-22/BL-000574.xlsx'
    print(KEY)
    
    s3_client = boto3.client('s3')
    try:
        response = s3_client.generate_presigned_url('get_object',Params={'Bucket': BUCKET_NAME, 'Key': KEY},ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL
    return response

    