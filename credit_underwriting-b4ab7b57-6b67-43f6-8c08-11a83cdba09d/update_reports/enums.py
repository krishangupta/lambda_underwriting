from enum import Enum



class Products(Enum):
    EEG_BL = "EEG-BL"
    EEG_LAP = "EEG-LAP"
    SME_BL = "SME-BL"
    SME_LAP = "SME-LAP"
    ML = "ML"
    EIL = "EIL"

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_

class ExcelSheetName(Enum):
    ASSESSED_LAP = "Assessed_LAP"
    ASSESSED_EIL = "Assessed_EIL"
    GTP = "GTP_LAP_BL"
    VIABILITY = "Viability"
    BANKING_SURROGATE = "Banking Surrogate"
    GST = "GST"
    SUMMARY = "SUMMARY"

class EligibilitySubPrograms(Enum):
    ASSESSED_INCOME_SME_LAP = "SME-LAP_Assessed_LAP"
    ASSESSED_INCOME_EIL = "EIL_Assessed_EIL"
    GTP_SME_BL = "SME-BL_GTP_LAP_BL"
    GTP_SME_LAP = "SME-LAP_GTP_LAP_BL"
    GTP_EIL = "EIL_GTP_LAP_BL"
    GTP_ML = "ML_GTP_LAP_BL"
    VIABILITY_ML = "ML_Viability"
    BANKING_SURROGATE_SME_LAP = "SME-LAP_Banking Surrogate"
    GST_SME_BL = "SME-BL_GST"