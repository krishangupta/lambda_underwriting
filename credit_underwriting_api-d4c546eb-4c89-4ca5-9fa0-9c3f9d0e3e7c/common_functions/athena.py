import json
import time
import boto3
import pandas as pd
import numpy as np
import io
import string

# Excel Formatting

class QueryAthena:

    def __init__(self, query, database):
        self.database = database
        self.folder = 'sme_underwriting/athena_output/'
        self.bucket = 'datalake-gsft'
        self.s3_output =  's3://' + self.bucket + '/' + self.folder
        self.region_name = 'ap-south-1'
        self.query = query


    def load_conf(self, q):
        try:
            self.client = boto3.client('athena', 
                              region_name = self.region_name
                              )
            response = self.client.start_query_execution(
                QueryString = q,
                    QueryExecutionContext={
                    'Database': self.database
                    },
                    ResultConfiguration={
                    'OutputLocation': self.s3_output,
                    }
            )
            self.filename = response['QueryExecutionId']
            print('Execution ID: ' + response['QueryExecutionId'])
            return response

        except Exception as e:
            print(e)


    def run_query(self):
        print('run_query ine 45')
        print(self.query)
        queries = [self.query]
        for q in queries:
            res = self.load_conf(q)
        try:
            query_status = None
            while query_status == 'QUEUED' or query_status == 'RUNNING' or query_status is None:
                query_status = self.client.get_query_execution(QueryExecutionId=res["QueryExecutionId"])['QueryExecution']['Status']['State']
                print(query_status)
                if query_status == 'FAILED' or query_status == 'CANCELLED':
                    raise Exception('Athena query with the string "{}" failed or was cancelled'.format(self.query))
                time.sleep(0.2)
            print('Query "{}" finished.'.format(self.query))

            df = self.obtain_data()
            return df

        except Exception as e:
            print(e)


    def obtain_data(self):
        try:
            self.resource = boto3.resource('s3', 
                                  region_name = self.region_name
                                  )
            response = self.resource \
            .Bucket(self.bucket) \
            .Object(key= self.folder + self.filename + '.csv') \
            .get()
            print("ran till here 74, common_functions")
            return pd.read_csv(io.BytesIO(response['Body'].read()), encoding='utf8')
        except Exception as e:
            print(e)
    
    
    def get_s3_file_name(self):
        try:
            return f"{self.s3_output + self.filename}.csv"
        except Exception as e:
            print(e)


class HelperClassS3:

    def copy_local_file_to_s3(self, local_file_path, local_file_name, s3_bucket, s3_path):
        try:
            s3_client = boto3.client('s3')
            s3_file_name = s3_path+local_file_name
            local_file_name = local_file_path+local_file_name
            s3_client.upload_file(local_file_name, s3_bucket, s3_file_name)
            return s3_file_name
        except Exception as ex:
            print(ex)


    def copy_s3_file_to_local(self, local_file_path, local_file_name, s3_bucket, s3_path):
        try:
            s3_client = boto3.client('s3')
            s3_file_name = s3_path+local_file_name
            local_file_name = local_file_path+local_file_name
            s3_client.download_file(s3_bucket, s3_file_name, local_file_name)
            return s3_file_name
        except Exception as ex:
            print(ex)