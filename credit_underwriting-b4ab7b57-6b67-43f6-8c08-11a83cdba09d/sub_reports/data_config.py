additional_fs_details_col_number_mapping = {
    "FY2021":"G",
    "FY2022":"F",
    "FY2023":"E"
}

additional_fs_details_row_number_mapping = {
  "CACashEqvTotal": 47,
  "CAOther_CATotal": 49,
  "CASundryDebTotal": 46,
  "CA_CITotal": 44,
  "CLOr_STLibOther_CLTotal": 26,
  "CLOr_STLib_STPTotal": 27,
  "Dep_Amo": 78,
  "ELShAppMoneyAll": 15,
  "EPSAftEIBasic": 141,
  "EPSAftEIDiluted": 142,
  "EPSBefEIBasic": 137,
  "EPSBefEIDiluted": 138,
  "EmployeeCosts": 72,
  "ExcepItemTotal": 84,
  "ExpsAuditorsFeesTotal": 74,
  "ExpsChangesInINVTotal": 71,
  "ExpsCost_MatConCost_MatCon": 69,
  "ExpsFinanceCostTotal": 77,
  "ExpsInsuranceExpsTotal": 75,
  "ExpsMgRemuTotal": 73,
  "ExpsOtherExpsGraTotOtExp": 79,
  "ExpsPowerAndFuelTotal": 76,
  "ExpsPurStckTr": 70,
  "ExtItemTotal": 90,
  "INV": 45,
  "In_TAIn_TANetTotal": 35,
  "LTB": 18,
  "Non_CADef_TA": 39,
  "Non_CAFixAssCapWIP": 36,
  "Non_CAFixAss_TAUnDev": 37,
  "Non_CANon_CITotal": 38,
  "Non_CAOtherNon_CATotal": 41,
  "Non_CA_LTLTotal": 40,
  "Non_CLOr_LTLibDef_TL": 19,
  "Non_CLOr_LTLibOther_LTLibTotal": 20,
  "Non_CLOr_LTLib_LTPTotal": 21,
  "OtherIncome": 64,
  "SPLBal_BF_PreYr": 108,
  "SPLINTOnCapital": 85,
  "SPLPartRemuSal": 86,
  "SPLPrDiscContOpTax": 100,
  "SPLTaxExpDisContOp": 102,
  "STB": 24,
  "STL": 48,
  "SalesRevOps": 62,
  "ShHolderFundMoneyAgSW": 12,
  "ShHolderFundShareCapitalTotal": 10,
  "ShHolderFund_res_surTotal": 11,
  "TA_TANetTotal": 34,
  "TaxExpCurrentTax": 95,
  "TaxExpDeferredTax": 96,
  "TradePayables": 25
}


set_revenue_value_mom_revenue_trend_col_mapping = {
  'Y1':'C',
  'Y2':'D',
  'Y3':'E',
  'FY1/FY2' : 'C',
  'FY2/FY3':'D',
  'estimate2020/21' : 'E'
}

set_revenue_value_mom_revenue_trend_row_mapping = {
  'audited_sales' : 7,
  'April' : 17,
  'May' : 18,
  'June' : 19,
  'July' : 20,
  'August' : 21,
  'September' : 22,
  'October' : 23,
  'November' : 24,
  'December' : 25,
  'January' : 26,
  'February' : 27,
  'March' : 28
}

set_revenue_value_revenue_estimation_col_mapping={
  'Year2018':'C',
  'Year2019':'D',
  'Year2020':'E'
}

set_revenue_value_revenue_estimation_row_mapping = {
  'audited_sales' : 7
}


rtr_sheet_statics = {
  'row_number_total' : 18,
  'start_row_rtr_content' : 20,
  'more_rows' : 500,
  'amount_2020_21':'AJ',
  'amount_2021_22':'AL'
}

set_gtp_values_col_mapping = {
  'Y1' : 'C',   #Y1
  'Y2' : 'D', #Y2
  'Y3' : 'E'  #Y3
}

set_gtp_values_row_mapping = {
  'profitbeforetax' : 8,
  'dep_amo' : 12,
  'exps_finance_cost' : 13
}


populate_customer_details_col_mapping = {
  'summary_col' : 'C'
}

populate_customer_details_row_mapping = {
  'login_date' : 4,
  'lead_code' : 5,
  'login_branch_name' : 6,

  'primary_applicant_name' : 8,
  'product' : 13,
  'loan_amount_required' : 14,
  'sales_manager' : 50,
  'dsa_name':51
}


populate_customer_details_applicants_col_mapping = {
  'customer_id' : 'E',
  'applicant_name' : 'C',
  'score' : 'H',
  'bucn_score' : 'J',
  'bs_score' : 'L',
  'bucm_score_decile' :'N',
  'gst_score_decile' : 'M',
  'fs_score_decile' : 'K'
  
}

populate_customer_details_static = {
  'starting_row' : 32,
}


fs_sheet_col_mapping = {
  'Y1':'E',
  'Y2':'F',
  'Y3':'G'
}

fs_sheet_row_mapping = {
  'equity_total' : 29,
  'assets_total' : 51,
  'total_revenue' : 66,
  'expenses_total' : 80,
  'profit_before_interest':82,
  'profit_before_extraordinary_items':88,
  'profit_before_tax':92,
  'profit_from_continuing_operations':98,
  'profit_discontinuing_op_after_tax':104,
  'profit_loss_period_or_profit_after_tax':106,
  'amount_avail_for_approp' : 110,
  'debt_or_equity' : 145,
  'current_ratio' : 146,
  'stock_days' : 147,
  'debtor_days' : 148,
  'creditor_days' : 149,
  'operating_cycle' : 150,
  'working_capital_requirement' : 151,
  'net_working_capital_reuirement' : 153,
  'interest_coverage_ratio' : 156,
  'dscr' : 158
  
}

fs_equity_mapping = {
  'start_row':10,
  'end_row':27
}
fs_assets_mapping = {
  'start_row':34,
  'end_row':49
}
fs_total_revenue_mapping = {
  'start_row':62,
  'end_row':64
}
fs_expenses_mapping = {
  'start_row':69,
  'end_row':79
}
fs_profit_before_interest = {
  #its total_revenue-expenses_total
}
fs_profit_before_extraordinary_items_mapping = {
  #profit_before_interest
  'start_row':84,
  'end_row':86
}
fs_profit_before_tax = {
  # =profit_before_extraordinaryitems - E90
  'arg2':90 
}
fs_profit_from_continuing_operations = {
  # profit_before_tax
  'start_row' : 95,
  'end_row' : 96
}
fs_profit_discontinuing_op_after_tax = {
  'arg1':100,
  'arg2':102
}

fs_amount_avail_for_approp = {
  'arg2':108
}

fs_debt_or_equity = {
  #additional_fs_details_row_number_mapping
  'start_row1':18,
  'end_row1':24,
  'start_row2':10,
  'end_row2':12
}

fs_current_ratio = {
  'start_row1':44,
  'end_row1':49,
  'start_row2':24,
  'end_row2':27
}

fs_stock_days = {
  'arg1':62,
  'arg2':45
}
fs_debtor_days = {
  'arg1':62,
  'arg2':46
}
fs_creditor_days = {
  'arg1':69,
  'arg2':25
}
fs_operating_cycle = {
  'arg1':147,
  'arg2':148,
  'arg3':149
}
fs_working_capital_requirement = {
  'arg1':66,
  'arg2':150
}
fs_net_working_capital_reuirement = {
  'arg1':151,
  'arg2':152
}
fs_interest_coverage_ratio = {
  'arg1':154,
  'arg2':155
}
fs_dscr = {
  'arg1':154,
  'arg2':157
}


revenue_estimation_col_mapping = {
  'growth_rate':'E',
  'fy2019_20':'C',
  'fy2020_21_estimate':'D',
  'GR_2019':'D',
  'GR_2020':'E',
  'Year2018':'C',
  'Year2019':'D',
  'Year2020':'E',
  'turnover_tringulation' : 'C',
  'estimated_2021':'F',
  'estimated_2022':'G'
}

RE_growth_rate_yearly = {
  'calc_row':7,
  'result_row':10
}

RE_growth_rate_mom_trend = {
  'start_row':17,
  'total_calc_row':29
}

RE_margin_estimation_EBITDA = {
  'obligations':45,
  'foir':46,
  'EBITDA':47,
  'EBITDA_margin':48,
  'sales_turnover':44,
  'sales_2020':7
}

RE_turnover_tringulation = {
  'turnover_as_per_books':35,
  'turnover_as_per_gst':36,
  'banking_credits':37,
  'gst_book_ratio':38,
  'banking_book_ratio':39,
  'banking_gst_ratio':40,
  
  'total_CT_for_current_acc':54,
  'total_CT_for_oc_od_acc':63
}


gtp_lap_bl_col_mapping = {
  'Year2018':'C',
  'Year2019':'D',
  'Year2020':'E',
  'Year2020_21_no_loan':'F',
  'Year2021_22_no_loan':'G',
  'Year2020_21_with_loan':'I',
  'Year2021_22_with_loan':'J'
}

gtp_lap_bl_row_mapping = {
  'margin':5,
  'sales_turnover':7,
  'profit_after_tax':11,
  'earnings_actuals':18,
  'multiplier':19,
  'earnings_considered':20,
  'other_income':21,
  'other_obligations':22,
  'gsft_emi':23,
  'foir1':24,
  'tenure':27,
  'rate':28,
  'amount':29,
  'rem_months':30,
  'EBITDA_margin':32,
  'DSCR_at_EBIDA':33,
  'haircut_revenue':38,
  'haircut_margin':39,
  'revenue_considered':40,
  'EBITDA_considered':41,
  'total_other_income':42,
  'foir2':42
}

gtp_lap_bl_profit_after_tax = {
  'profit_before_tax':8,
  'non_buss_income':9,
  'tax_paid':10
}

gtp_lap_bl_earning_actuals = {
  'sr1':11,
  'er1':13,
  'sr2':15,
  'er2':17,
  'arg':14
}

rtr_col_mapping = {
  'year':'C',
  'amount':'D',
  'loan_amount':'F',
  'POS':'J',
}

rtr_row_mapping = {
  'y1':3,
  'y2':4,
  'y3':5,
  'y4':6,
  'y5':7,
  'y6':8
}

case_details_mapping = {
  'mv_as_per_customer':'E',
  'res_row':61,
  'sr':52,
  'er':60
}

school_data_row_mapping = {
'Nur':4,
'LKG':5,
'UKG':6,
'1st':7,
'2nd':8,
'3rd':9,
'4th':10,
'5th':11,
'6th':12,
'7th':13,
'8th':14,
'9th':15,
'10th':16,
'11th':17,
'12th':18,

}

school_data_col_mapping = {
  'students_y1' : 'B',
  'students_y2' : 'C',
  'students_y3' : 'D',
  'ann_charges_y1' : 'E',
  'ann_charges_y2' : 'F',
  'ann_charges_y3' : 'G',
  'tuition_y1' : 'H',
  'tuition_y2' : 'I',
  'tuition_y3' : 'J',
  
}

school_db_field_map = {
'1st' : {
'students' : 'total_no_of_students_class_1',
'yearly_development_charges' : 'p_yearly_development_charges',
'tuition_fee' : 'p_tuition_fee',
'other_charges' : 'p_other_charges_for_facilities'
},
'2nd' : {
'students' : 'total_no_of_students_class_2',
'yearly_development_charges' : 'p_yearly_development_charges',
'tuition_fee' : 'p_tuition_fee',
'other_charges' : 'p_other_charges_for_facilities'
},
'3rd': {
'students' : 'total_no_of_students_class_3',
'yearly_development_charges' : 'p_yearly_development_charges',
'tuition_fee' : 'p_tuition_fee',
'other_charges' : 'p_other_charges_for_facilities'
},
'4th': {
'students' : 'total_no_of_students_class_4',
'yearly_development_charges' : 'p_yearly_development_charges',
'tuition_fee' : 'p_tuition_fee',
'other_charges' : 'p_other_charges_for_facilities'
},
'5th': {
'students' : 'total_no_of_students_class_5',
'yearly_development_charges' : 'p_yearly_development_charges',
'tuition_fee' : 'p_tuition_fee',
'other_charges' : 'p_other_charges_for_facilities'
},
'6th': {
'students' : 'total_no_of_students_class_6',
'yearly_development_charges' : 'm_yearly_development_charges',
'tuition_fee' : 'm_tuition_fee',
'other_charges' : 'm_other_charges_for_facilities'
},
'7th': {
'students' : 'total_no_of_students_class_7',
'yearly_development_charges' : 'm_yearly_development_charges',
'tuition_fee' : 'm_tuition_fee',
'other_charges' : 'm_other_charges_for_facilities'
},
'8th': {
'students' : 'total_no_of_students_class_8',
'yearly_development_charges' : 'm_yearly_development_charges',
'tuition_fee' : 'm_tuition_fee',
'other_charges' : 'm_other_charges_for_facilities'
},
'9th': {
'students' : 'total_no_of_students_class_9',
'yearly_development_charges' : 's_yearly_development_charges',
'tuition_fee' : 's_tuition_fee',
'other_charges' : 's_other_charges_for_facilities'
},
'10th': {
'students' : 'total_no_of_students_class_10',
'yearly_development_charges' : 's_yearly_development_charges',
'tuition_fee' : 's_tuition_fee',
'other_charges' : 's_other_charges_for_facilities'
},
'11th': {
'students' : 'total_no_of_students_class_11',
'yearly_development_charges' : 'ss_yearly_development_charges',
'tuition_fee' : 'ss_tuition_fee',
'other_charges' : 'ss_other_charges_for_facilities'
},
'12th': {
'students' : 'total_no_of_students_class_12',
'yearly_development_charges' : 'ss_yearly_development_charges',
'tuition_fee' : 'ss_tuition_fee',
'other_charges' : 'ss_other_charges_for_facilities'
}
}
