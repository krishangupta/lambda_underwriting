import json
import boto3
import time
import botocore
import pandas as pd
import pandas
import datetime as dt

#LOCAL IMPORTS
from common_functions.constants import function_arn
from common_functions.athena import QueryAthena

def query_handler(query, database, filter_clause, filter_variable, filter_required=False):
    print('query_handler line 8')
    if filter_required:
        filter_query = f"\nWHERE {filter_clause}='{filter_variable}'"
        query = query + filter_query

    athena_handler = QueryAthena(query=query, database=database)
    df = athena_handler.run_query()
    return df

def get_report(body):
    try:
        print("in get report/ at ", dt.datetime.now())
        
        loan_id = body.get('loan_id')
        cam_type = body.get('cam_type', '')
        stage = body.get('stage', 'PROD')
        callback_url = body.get('callback_url', 'https://odin.staging.growth-source.com:443/odin/api/v1/camInsights/camUrlCallback')
        source = body.get('source', '')
        
        payload = {
            'stage' : stage,
            'cam_type' : cam_type,
            'loan_id' : loan_id,
            'callback_url' : callback_url,
            'source' : source
        }
        
        if(stage == 'UAT'):
            print("IN satge = UAT")
            #payload['stage'] = 'PROD'
            client = boto3.client('lambda')
            response = client.invoke(
                FunctionName = 'arn:aws:lambda:ap-south-1:247126901804:function:credit_underwriting',
                #InvocationType = 'RequestResponse',
                InvocationType = 'Event',
                Payload = json.dumps(payload)
            )
            # responseFromChild = json.load(response['Payload'])
            # print('\n')
            # print("responseFromChild", responseFromChild)
            
            # if(responseFromChild['error']):
            #     return {
            #         "status": responseFromChild['errorMsg'],
            #         "error": True
            #     }
            
            return {
                    "status": "Success!",
                    #"cam_url": responseFromChild['data']['cam_url'],
                    "error": False
                }
        else:
            query = "SELECT lead_code FROM tp_source_db.cons_cibil_info WHERE lead_code = '"+ loan_id +"'"
            df = query_handler(query=query, database='default', filter_clause='', filter_variable='', filter_required=False)
            
            fileStatus = {}
            if(len(df)>0):
                client = boto3.client('lambda')
                response = client.invoke(
                    FunctionName = function_arn,
                    InvocationType = 'Event',
                    Payload = json.dumps(payload)
                )
                # x = client.invoke(
                #         FunctionName='arn:aws:lambda:ap-south-1:247126901804:function:credit_underwriting:29',
                #         InvocationType = "Event",
                #         Payload = bytes(json.dumps(payload), encoding='utf-8'))
                #responseFromChild = json.load(response['Payload'])
                #print('\n')
                #print("responseFromChild",responseFromChild)
            
                print(f"in get report/ after invoking lambda credit_underwriting for {payload.get('lead_code', '')} at {dt.datetime.now()}")
                fileStatus =  {
                    "status":"Success!",
                    "error": False
                }
                
            else:
                fileStatus = {
                    "status": "Bureau Details not found",
                    "error": True
                }
            
            return fileStatus
    except Exception as e:
        print(f"Exception Occured: {str(e)}")
        return  {
                    "status": str(e),
                    "error": True
                }
    

    
    