from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.styles import PatternFill, Border, Side, Alignment, Font
import openpyxl
import json
import time
import boto3
import pandas as pd
import numpy as np
import datetime as dt
import io
import string

# Excel Formatting

measurer = np.vectorize(len)

font = Font(name='Calibri',
                size=11,
                 bold=False,
                 italic=False,
                 vertAlign=None,
                 underline='none',
                 strike=False,
                 color='FF000000')

font_h = Font(name='Ariel Black',
                size=13,
                 bold=True,
                 italic=False,
                 vertAlign=None,
                 underline='none',
                 strike=False,
                 color='FFB22222')


fill = PatternFill(fill_type=None,
                 start_color='FFFFFFFF',
                 end_color='FF000000')
                 
conditional_fill = PatternFill(start_color='ffc7ce', end_color='ffc7ce', fill_type='solid')

number_format = 'General'


def copy_df_to_excel_with_formatting(df, ws, start_row=0, start_column=0, auto_filter_row=None):
    df.replace(np.nan, '', regex=True, inplace=True)
    rows = dataframe_to_rows(df, header=True, index=False)
    
    for r_idx, row in enumerate(rows, 1):
        if r_idx == 1:
            for x, y in zip(range(len(df.columns.values)), string.ascii_uppercase):    
                if x > 0:
                    ws.column_dimensions[y].width = max(12,len(str(row[x]))+3)   
        for c_idx, value in enumerate(row, 1):
            cell = ws.cell(row= r_idx+start_row, column=c_idx+start_column)
            if r_idx == 1:
                cell.font = font_h             
                cell.fill = conditional_fill 
            # elif c_idx == 15:
            #     cell.fill = conditional_fill
            else:
                cell.font = font             
            ws.cell(row=r_idx+start_row, column=c_idx+start_column, value=value)

    if auto_filter_row is None:
        ws.auto_filter.ref = ws.dimensions
    elif auto_filter_row=='RTR':
        #FullRange = "A1:" + openpyxl.utils.get_column_letter(ws.max_column) + str(ws.max_row)
        ws.auto_filter.ref = "A19:AL19"
    return ws
    
def copy_df_to_excel_with_formatting_rtr(df,ws, start_row=0, start_column=0, auto_filter_row=None):
    df.replace(np.nan, '', regex=True, inplace=True)
    rows = dataframe_to_rows(df, header=True, index=False)
    
    for r_idx, row in enumerate(rows, 1):
        if r_idx == 1:
            for x, y in zip(range(len(df.columns.values)), string.ascii_uppercase):    
                if x > 0:
                    ws.column_dimensions[y].width = max(12,len(str(row[x]))+3)   
        for c_idx, value in enumerate(row, 1):
            cell = ws.cell(row= r_idx+start_row, column=c_idx+start_column)
            if r_idx == 1:
                cell.font = font_h             
                cell.fill = conditional_fill 
            elif c_idx == 14 and (int(value)>20 or int(value)<10):
                cell.fill = conditional_fill
            else:
                cell.font = font             
            ws.cell(row=r_idx+start_row, column=c_idx+start_column, value=value)

    if auto_filter_row is None:
        ws.auto_filter.ref = ws.dimensions
    elif auto_filter_row=='RTR':
        #FullRange = "A1:" + openpyxl.utils.get_column_letter(ws.max_column) + str(ws.max_row)
        ws.auto_filter.ref = "A19:AL19"
    return ws


def copy_cell_value_openpyxl(ws, r_idx, c_idx, value):
    cell = ws.cell(row= r_idx , column=c_idx)
    cell.font = font
    ws.cell(row=r_idx, column=c_idx, value=value)
    return ws


class QueryAthena:

    def __init__(self, query, database):
        self.database = database
        self.folder = 'sme_underwriting/athena_output/'
        self.bucket = 'datalake-gsft'
        self.s3_output =  's3://' + self.bucket + '/' + self.folder
        self.region_name = 'ap-south-1'
        self.query = query


    def load_conf(self, q):
        try:
            self.client = boto3.client('athena', 
                              region_name = self.region_name
                              )
            response = self.client.start_query_execution(
                QueryString = q,
                    QueryExecutionContext={
                    'Database': self.database
                    },
                    ResultConfiguration={
                    'OutputLocation': self.s3_output,
                    }
            )
            self.filename = response['QueryExecutionId']
            print('Execution ID: ' + response['QueryExecutionId'])
            return response

        except Exception as e:
            print(e)


    def run_query(self):
        queries = [self.query]
        for q in queries:
            res = self.load_conf(q)
        try:
            query_status = None
            while query_status == 'QUEUED' or query_status == 'RUNNING' or query_status is None:
                query_status = self.client.get_query_execution(QueryExecutionId=res["QueryExecutionId"])['QueryExecution']
                #print(query_status)
                #query_status = query_status['Status']['State']
                if query_status['Status']['State'] == 'FAILED' or query_status['Status']['State'] == 'CANCELLED':
                    raise Exception(f'Athena query with the string "{self.query}" failed or was cancelled with error {query_status}')
                time.sleep(10)
            print('Query "{}" finished.'.format(self.query))

            df = self.obtain_data()
            return df

        except Exception as e:
            print('Exception occurred in run_query', str(e))


    def obtain_data(self):
        time.sleep(10)
        try:
            self.resource = boto3.resource('s3', 
                                  region_name = self.region_name
                                  )
            print("---::: MK EXECUTION ID ->>> ", self.filename)
            print("----::: Sleeping for 2 secs :::-----")
            time.sleep(2)
            response = self.resource \
            .Bucket(self.bucket) \
            .Object(key= self.folder + self.filename + '.csv') \
            .get()
            return pd.read_csv(io.BytesIO(response['Body'].read()), encoding='utf8')
        except Exception as e:
            print("Exception Occured in obtain_data", str(e))
    
    
    def get_s3_file_name(self):
        try:
            return f"{self.s3_output + self.filename}.csv"
        except Exception as e:
            print(e)


class HelperClassS3:

    def copy_local_file_to_s3(self, local_file_path, local_file_name, s3_bucket, s3_path):
        try:
            s3_client = boto3.client('s3')
            s3_file_name = s3_path+local_file_name
            local_file_name = local_file_path+local_file_name
            s3_client.upload_file(local_file_name, s3_bucket, s3_file_name)
            return s3_file_name
        except Exception as ex:
            print(ex)


    def copy_s3_file_to_local(self, local_file_path, local_file_name, s3_bucket, s3_path):
        try:
            s3_client = boto3.client('s3')
            s3_file_name = s3_path+local_file_name
            local_file_name = local_file_path+local_file_name
            s3_client.download_file(s3_bucket, s3_file_name, local_file_name)
            return s3_file_name
        except Exception as ex:
            print(ex)
            

def log_fails(json_message, loan_id):
    bucket_name = 'datalake-gsft'
    log_file =f'/tmp/{loan_id}.json'
    local_file_path = '/'.join(log_file.split('/')[:-1])+'/'
    local_file_name = log_file.split('/')[-1]

    todays_date = str(dt.datetime.today().date())

    with open(log_file, 'w', encoding='utf-8') as f:
        json.dump(json_message, f, ensure_ascii=False, indent=4)
        print(json_message)
        print(log_file)
    
    s3_helper = HelperClassS3()
    s3_file_name = 's3://' + bucket_name + '/' + s3_helper.copy_local_file_to_s3(local_file_path, local_file_name, bucket_name, f'sme_underwriting/exceptions/{todays_date}/')
    