import requests
import os
import time
import json

from commons.config import (CLIENT_ID, AUTHORITY_URL, USERNAME, PASSWORD, SCOPES, RESOURCE_URL, API_VERSION)

def response_handler(error, errorMsg, data):
    return {
        'errorMsg': errorMsg,
        'error': error,
        'data': data
    }


def meraki_cam_callback(body, cam_url, error, message):
    try:
        
        # print("going to call PROD API")
        # temp_url = "https://odin.prod.growth-source.com/odin/api/v1/keycloak/screens"
        # temp_res = requests.get(temp_url)
        # print(temp_res.json())
        
        response_data = {}
        
        response_data['lead_code'] = body['loan_id']
        response_data['cam_url'] = cam_url
        
        url = body['callback_url']
        print("Trying to Call api", url)
        payload = {
            "leadCode" : body['loan_id'],
            "camUrl" : cam_url,
            "error": error,
            "message" : message
        }
        print("payload", payload)
        
        r = requests.post(url, json=payload)
        print(r.json())
        if not r.status_code == 200:
            print(f"update cam_url failed with status code {r.status_code}")
            return response_handler(True, 'Failed to Save camUrl', None)
        
        
        return response_handler(False, None, response_data)
    except Exception as e:
        print(f"Exception Occured: {str(e)}")
        return response_handler(True, str(e), None)
        
        
def push_to_one_drive(file_name, destination, headers, path):
    try:
        file_name = str(file_name) + '.xlsx'

        onedrive_destination = '{}{}/me/drive/root:/cognos/'.format(RESOURCE_URL,API_VERSION)
        onedrive_destination = onedrive_destination + destination
        
        file_data = open(path, 'rb')
        file_data.seek(0,os.SEEK_END)
        
        file_size = file_data.tell()
        file_data.seek(0,0)
        
        time.sleep(0.1)
        if file_size < 4100000: 
            #Perform a simple upload to the API
            r = requests.put(onedrive_destination+"/"+file_name+":/content", data=file_data, headers=headers)
            file_data.close() 
            
            # print(r.json())
            
            if not r.status_code in (200, 201):
                print(r.json())
                if r.status_code == 423:
                    return response_handler(True, "Failed to push CAM to OneDrive. File needs to be closed from all devices first.", None)
                return response_handler(True, "Failed to push CAM to OneDrive", None)
            return response_handler(False, None, r.json())
        else:
            #Creating an upload session
            upload_session = requests.post(onedrive_destination+"/"+file_name+":/createUploadSession", headers=headers).json()
            chunk_data_upload = None
            with open(path, 'rb') as f:
                print('File > 4Mb')
                total_file_size = os.path.getsize(path)
                chunk_size = 327680
                chunk_number = total_file_size//chunk_size
                chunk_leftover = total_file_size - chunk_size * chunk_number
                i = 0
                while True:
                    chunk_data = f.read(chunk_size)
                    start_index = i*chunk_size
                    end_index = start_index + chunk_size
                    # If end of file, break
                    if not chunk_data:
                        break
                    if i == chunk_number:
                        end_index = start_index + chunk_leftover
                    # Setting the header with the appropriate chunk data location in the file
                    headers = {'Content-Length':'{}'.format(chunk_size),'Content-Range':'bytes {}-{}/{}'.format(start_index, end_index-1, total_file_size)}
                    # Upload one chunk at a time
                    chunk_data_upload = requests.put(upload_session['uploadUrl'], data=chunk_data, headers=headers)
                    print(chunk_data_upload)
                    print(chunk_data_upload.json())
                    i = i + 1
                file_data.close() 
            if not chunk_data_upload.status_code in (200, 201):
                print(chunk_data_upload.json())
                if chunk_data_upload.status_code == 423:
                    return response_handler(True, "Failed to push CAM to OneDrive. File needs to be closed from all devices first.", None)
                return response_handler(True, "Failed to push CAM to OneDrive", None)
            print('Last chunk details being returned')
            return response_handler(False, None, chunk_data_upload.json())
            
    except Exception as e:
        print(str(e))
        return response_handler(True, str(e), None)
        
        
def get_from_one_drive(file_name, destination, headers):
    if(destination == 'end_of_search'):
        print(f"> File {file_name} not found in all 3 folders")
        return response_handler(True, "CAM Not Found", None)
    try:
        file_name = str(file_name)

        onedrive_destination = '{}{}/me/drive/root:/cognos/'.format(RESOURCE_URL,API_VERSION)
        onedrive_destination = onedrive_destination + destination

        time.sleep(0.1)
        r = requests.get(onedrive_destination+file_name+".xlsx", headers=headers)
        if r.status_code == 200:
            #print('\n> Response Success')
            response = r.json()
            print(response)
            download_url = response['@microsoft.graph.downloadUrl']
            time.sleep(0.1)
            r = requests.get(download_url, allow_redirects=True)
            if r.status_code == 200:
                with open(f'/tmp/{file_name}.xlsx', 'wb') as File:
                    File.write(r.content)
                print(f'> File {file_name} Saved on Local from {destination}')
                return response_handler(False, None, {"source": destination})
            else:
                print('> Failed: {} with status code {}'.format(file_name, r.status_code))
                return response_handler(True, "failed to read CAM", None)
        else:
            if(destination == 'sme_underwriting/sme_dashboard_staging/'):
                return get_from_one_drive(file_name, 'sme_underwriting/SME Rejected/', headers)
            elif(destination == 'sme_underwriting/SME Rejected/'):
                return get_from_one_drive(file_name, 'sme_underwriting/SME Approved/', headers)
            elif(destination == 'sme_underwriting/SME Approved/'):
                return get_from_one_drive(file_name, 'end_of_search', headers)
            else:
                print(f"{file_name} not found/failed")
                return response_handler(True, "CAM Not Found", None)
    except Exception as e:
        print(e)
        return response_handler(True, str(e), None)
        #print('\n> Failed: {} case {}'.format(r.status_code, file_name))
    
    