loan_level_rtr_query = """ SELECT lead_code,
       cct.customer_id as customer_id,
       gsft_request_id,
       bureau,
       date_reported,
       disbursed_dt,
       ownership,
       last_payment_date,
       close_dt,
       acct_in_dispute,
       original_term,
       account_status,
       suit_filed_wilful_default,
       write_off_setteled_status,
       write_off_total,
       write_off_principal,
       settlement_amt,
       credit_limit,
       cash_limit,
       installment_amt,
       installment_frequency,
       security_status,
       disbursed_amt,
       last_payment_amount,
       actual_payment_amount,
       current_balance,
       amount_overdue,
       account_type,
       sanctioned_amount,
       matched_type,
       payment_hist_month_1,
       payment_hist_dpd_1,
       payment_hist_status_1,
       payment_hist_month_2,
       payment_hist_dpd_2,
       payment_hist_status_2,
       payment_hist_month_3,
       payment_hist_dpd_3,
       payment_hist_status_3,
       payment_hist_month_4,
       payment_hist_dpd_4,
       payment_hist_status_4,
       payment_hist_month_5,
       payment_hist_dpd_5,
       payment_hist_status_5,
       payment_hist_month_6,
       payment_hist_dpd_6,
       payment_hist_status_6,
       payment_hist_month_7,
       payment_hist_dpd_7,
       payment_hist_status_7,
       payment_hist_month_8,
       payment_hist_dpd_8,
       payment_hist_status_8,
       payment_hist_month_9,
       payment_hist_dpd_9,
       payment_hist_status_9,
       payment_hist_month_10,
       payment_hist_dpd_10,
       payment_hist_status_10,
       payment_hist_month_11,
       payment_hist_dpd_11,
       payment_hist_status_11,
       payment_hist_month_12,
       payment_hist_dpd_12,
       payment_hist_status_12,
       payment_hist_month_13,
       payment_hist_dpd_13,
       payment_hist_status_13,
       payment_hist_month_14,
       payment_hist_dpd_14,
       payment_hist_status_14,
       payment_hist_month_15,
       payment_hist_dpd_15,
       payment_hist_status_15,
       payment_hist_month_16,
       payment_hist_dpd_16,
       payment_hist_status_16,
       payment_hist_month_17,
       payment_hist_dpd_17,
       payment_hist_status_17,
       payment_hist_month_18,
       payment_hist_dpd_18,
       payment_hist_status_18,
       payment_hist_month_19,
       payment_hist_dpd_19,
       payment_hist_status_19,
       payment_hist_month_20,
       payment_hist_dpd_20,
       payment_hist_status_20,
       payment_hist_month_21,
       payment_hist_dpd_21,
       payment_hist_status_21,
       payment_hist_month_22,
       payment_hist_dpd_22,
       payment_hist_status_22,
       payment_hist_month_23,
       payment_hist_dpd_23,
       payment_hist_status_23,
       payment_hist_month_24,
       payment_hist_dpd_24,
       payment_hist_status_24,
       payment_hist_month_25,
       payment_hist_dpd_25,
       payment_hist_status_25,
       payment_hist_month_26,
       payment_hist_dpd_26,
       payment_hist_status_26,
       payment_hist_month_27,
       payment_hist_dpd_27,
       payment_hist_status_27,
       payment_hist_month_28,
       payment_hist_dpd_28,
       payment_hist_status_28,
       payment_hist_month_29,
       payment_hist_dpd_29,
       payment_hist_status_29,
       payment_hist_month_30,
       payment_hist_dpd_30,
       payment_hist_status_30,
       payment_hist_month_31,
       payment_hist_dpd_31,
       payment_hist_status_31,
       payment_hist_month_32,
       payment_hist_dpd_32,
       payment_hist_status_32,
       payment_hist_month_33,
       payment_hist_dpd_33,
       payment_hist_status_33,
       payment_hist_month_34,
       payment_hist_dpd_34,
       payment_hist_status_34,
       payment_hist_month_35,
       payment_hist_dpd_35,
       payment_hist_status_35,
       payment_hist_month_36,
       payment_hist_dpd_36,
       payment_hist_status_36,
       account_code,
       retail_trade,
       real_estate_trade,
       installment_trade,
       cur_mop,
       wor_mop,
       credit_line,
       account_index,
       months_since_update,
       months_since_open,
       closed_trade,
       mortgage_trade,
       revolving_trade,
       months_since_close,
       closed_good_trade,
       secured_trade,
       months_since_chargeoff,
       tenure_estimated,
       rate_estimated,
       emi_amount_estimated,
       emi_estimation_method,
       cci.applicant_name as applicant_name,
       cci.score_value as cibil_score
FROM tp_source_db.cons_cibil_tradelines cct
LEFT JOIN (
  SELECT customer_id, 
         applicant_name,
         score_value
  FROM tp_source_db.cons_cibil_info cci
  WHERE lead_code = '<loan_no>'
  GROUP BY 1, 2, 3
  ) cci ON cct.customer_id = cci.customer_id
WHERE lead_code = '<loan_no>'
"""

loan_level_gst_month_query = """ SELECT lead_code,
       gstin,
       fy,
       category,
       fy_category,
       tax,
       ttl_tax,
       ttl_val,
       date(ret_period) as ret_period
FROM "tp_source_db"."gst_sales_and_purchase_month"
"""

loan_level_gst_state_query = """ SELECT lead_code,
       gstin,
       fy,
       category,
       fy_category,
       state_name,
       state_code,
       ttl_igst,
       ttl_cgst,
       tax,
       ttl_cess,
       ttl_tax,
       ttl_val,
       ttl_rec,
       ttl_sgst
FROM "tp_source_db"."gst_sales_and_purchase_state"
"""


loan_level_bre_query  = """
SELECT loan_number,
       customer_id,
       applicant_type,
       stage,
       active_status,
       approval_status,
       deviation_val
FROM "los_afx_db"."sme_eeg_bre_deviation"
"""


loan_level_gsft_score_query = """
SELECT lead_code,
       customer_id,
       variable_name,
       variable_explanation,
       variable_max_score,
       variable_min_score,
       variable_value,
       variable_score,
       variable_difftomax,
       partner_name
FROM "gsft-analytics-db"."cons_cibil_gsft_score_detail"
"""

loan_level_bank_score_query = f"""
SELECT * 
FROM "gsft-analytics-db"."bsmt_gsft_score_detail" 
where lead_code = '<loan_no>'
"""

commercial_score  = """
SELECT lead_code,
       customer_id,
       name,
       score_value,
       score_comments
FROM "tp_source_db"."comm_crif_info"
WHERE lead_code = '<loan_no>'
"""

loan_level_fs_data_query = """
SELECT fy,
       customer_id,
       lead_code,
       ltb,
       stb,
       debtserv,
       employeecosts,
       wcgap,
       netfixass,
       totalnon_ca,
       inv,
       casheqv,
       other_ca,
       shortterminvest,
       stl,
       totalassets,
       total_ca,
       totlib,
       longtermdebt,
       shorttermdebt,
       sales,
       ebitda,
       totalexps,
       otherincome,
       tax,
       totalincome,
       pat,
       dep_amo,
       profitbeforetax,
       grossmargin,
       cashprofitmargin,
       opprratio,
       netmargin,
       ca_tr,
       debt_dcp,
       tatratio,
       fixasstr,
       wcturnoverratio,
       cred_dcpp,
       intcovr,
       debtratio,
       debteqratio,
       eqratio,
       dscr,
       qrexinv6m,
       currentratio,
       ExpsFinanceCostTotal
FROM "tp_source_db"."financial_statement_data"
"""

loan_level_banking_query = """ WITH cte AS
  (SELECT year(txndate) AS YEAR,
          month(txndate) AS MONTH,
          gsft_transaction_category,
          sum(txn_amount) AS txnamount
   FROM tp_source_db.bs_account_transactions
   WHERE lead_code = '<loan_no>'
   GROUP BY 1,
            2,
            3)
SELECT *
FROM cte;"""



cust_level_scores = """
SELECT distinct
       cci.customer_id,
       cci.applicant_name,
       gsft.gsft_score,
       gsft.gsft_score_decile,
       cci.score_value as cibil_score
FROM "tp_source_db"."cons_cibil_info" cci
LEFT JOIN "gsft-analytics-db"."cons_cibil_gsft_score_master" gsft
ON gsft.customer_id = cci.customer_id
WHERE cci.lead_code = '<loan_no>'
"""


cust_details = """
SELECT DISTINCT 
                login_date,
                lead_code,
                lead_name,
                login_branch_name,
                product,
                primary_applicant_name,
                industry,                
                loan_amount_required,
                sales_manager,
                dsa_name
FROM  "dbt_cam_data"."sme_eeg_application_t" 
WHERE lead_code = '<loan_no>'
"""


additional_fs_details = f"""
SELECT
fy,
ShHolderFundShareCapitalTotal,
ShHolderFund_res_surTotal,
ShHolderFundMoneyAgSW,
ELShAppMoneyAll,
LTB,
Non_CLOr_LTLibDef_TL,
Non_CLOr_LTLibOther_LTLibTotal,
Non_CLOr_LTLib_LTPTotal,
STB,
TradePayables,
CLOr_STLibOther_CLTotal,
CLOr_STLib_STPTotal,
TA_TANetTotal,
In_TAIn_TANetTotal,
Non_CAFixAssCapWIP,
Non_CAFixAss_TAUnDev,
Non_CANon_CITotal,
Non_CADef_TA,
Non_CA_LTLTotal,
Non_CAOtherNon_CATotal,
CA_CITotal,
INV,
CASundryDebTotal,
CACashEqvTotal,
STL,
CAOther_CATotal,
SalesRevOps,
OtherIncome,
ExpsCost_MatConCost_MatCon,
ExpsPurStckTr,
ExpsChangesInINVTotal,
EmployeeCosts,
ExpsMgRemuTotal,
ExpsAuditorsFeesTotal,
ExpsInsuranceExpsTotal,
ExpsPowerAndFuelTotal,
ExpsFinanceCostTotal,
Dep_Amo,
ExpsOtherExpsGraTotOtExp,
ExcepItemTotal,
SPLINTOnCapital,
SPLPartRemuSal,
ExtItemTotal,
TaxExpCurrentTax,
TaxExpDeferredTax,
SPLPrDiscContOpTax,
SPLTaxExpDisContOp,
SPLBal_BF_PreYr,
EPSBefEIBasic,
EPSBefEIDiluted,
EPSAftEIBasic,
EPSAftEIDiluted
FROM "tp_source_db"."financial_statement_data"
WHERE lead_code = '<loan_no>'
"""


loan_level_comm_rtr_query = f"""
SELECT ct.*,ci.name,ci.score_value FROM "tp_source_db"."comm_crif_tradelines" ct
left join "tp_source_db"."comm_crif_info" ci
on ct.customer_id = ci.customer_id
where ct.lead_code = '<loan_no>'
"""

loan_level_pos_query = f"""
SELECT * FROM "gsft-analytics-db"."cons_cibil_pos" WHERE lead_code = '<loan_no>'
"""

get_all_applicants_customer_id = f"""
SELECT * 
FROM "dbt_cam_data"."sme_eeg_applicants_t" 
where lead_code = '<loan_no>'
"""

bucn_score_decile = f"""
SELECT customer_id, gsft_score_decile 
FROM "gsft-analytics-db"."cons_cibil_gsft_score_master" 
where customer_id in (<customer_no>)
"""

bucm_score_decile = f"""
SELECT customer_id, gsft_score_decile 
FROM "gsft-analytics-db"."comm_crif_gsft_score_master" 
where customer_id in (<customer_no>)
"""

gst_score_decile = f"""
SELECT customer_id, score_decile 
FROM "gsft-analytics-db"."gst_gsft_score_master" 
where customer_id in (<customer_no>)
"""

fs_score_decile = f"""
SELECT customer_id, gsft_score_decile 
FROM "gsft-analytics-db"."fs_master_score" 
where customer_id in (<customer_no>)
"""

bank_score_decile = f"""
SELECT customer_id,gsft_score_decile 
FROM "gsft-analytics-db"."bsmt_gsft_score_master" 
where customer_id in (<customer_no>)
"""

cbse_school_data = f"""
SELECT * FROM "gsft-analytics-db"."cbse_schools_db" where affiliation_code = '<cbse_aff_id>'
"""

credit_queue_date = f"""
SELECT lead_code, updt_dt FROM "afx_master"."lead_deviationmas" 
where lead_code = '<loan_no>' and approve_status = 'Y' and stage = 'PREQUAL'
"""

address_details = f"""
SELECT main.lead_code,
concat(main.office_address , case when main.office_address_pincode is null then ' ' else cast(cast(main.office_address_pincode as integer) as varchar) end ) as office_address,
concat(co.residential_address , case when co.residential_address_pincode is null then ' ' else cast(cast(co.residential_address_pincode as integer) as varchar) end ) as residential_address
FROM "dbt_cam_data"."sme_eeg_applicants_t" main
left join ( select lead_code , residential_address,residential_address_pincode  FROM "dbt_cam_data"."sme_eeg_applicants_t" where applicant_role <> 'M' order by customer_id ) co
on main.lead_code = co.lead_code
where main.lead_code = '<loan_no>'and main.applicant_role = 'M'
LIMIT 1
"""

rag_classification = f"""
SELECT distinct lead_code, classification FROM "sme_credit_data"."sme_credit_queue_classif" 
where lead_code = '<loan_no>'
"""