class ExcelAxis:
    def __init__(self, row, column) -> None:
        self.row = row
        self.column = column

class EligibilityProgramFields:
    def __init__(self, loanAmount, roi, tenure, currentYearFoir, nextYearFoir, finalAbb, abbEmiRatio) -> None:
        self.loanAmount = loanAmount
        self.roi = roi
        self.tenure = tenure
        self.currentYearFoir = currentYearFoir
        self.nextYearFoir = nextYearFoir
        self.finalAbb = finalAbb
        self.abbEmiRatio = abbEmiRatio
    
    @classmethod
    def get_by_key(self, key, coordinatesContainer):
        switcher = {
            'loanAmount': coordinatesContainer.loanAmount,
            'roi': coordinatesContainer.roi,
            'tenure': coordinatesContainer.tenure,
            'currentYearFoir': coordinatesContainer.currentYearFoir,
            'nextYearFoir': coordinatesContainer.nextYearFoir,
            'finalAbb': coordinatesContainer.finalAbb,
            'abbEmiRatio': coordinatesContainer.abbEmiRatio
        }
        return switcher.get(key, None)
 
def response_handler(error, errorMsg, data):
    return {
        'errorMsg': errorMsg,
        'error': error,
        'data': data
    }