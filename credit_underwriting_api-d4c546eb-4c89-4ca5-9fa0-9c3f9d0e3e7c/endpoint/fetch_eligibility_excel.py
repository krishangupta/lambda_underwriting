import json
import boto3
import time
import botocore

#LOCAL IMPORTS
from common_functions.constants import function_arn

client = boto3.client('lambda')

def fetch_eligibility_excel(body):
    print("in fetch_eligibility_excel")
    
    
    response = client.invoke(
        FunctionName = function_arn,
        InvocationType = 'RequestResponse',
        Payload = json.dumps(body)
    )
 
    responseFromChild = json.load(response['Payload'])
 
    print('\n')
    print(responseFromChild)
    
    
    return responseFromChild