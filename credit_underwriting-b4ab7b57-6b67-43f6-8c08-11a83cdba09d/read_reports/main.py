import openpyxl
import msal
import time
import requests
import os

#LOCAL IMPORTS
import read_reports.constants as c

from commons.constants import uat_lead_code
from commons.utils import get_from_one_drive, push_to_one_drive, response_handler
from commons.config import (CLIENT_ID, AUTHORITY_URL, USERNAME, PASSWORD, SCOPES, RESOURCE_URL, API_VERSION)


def calculate_fields(response_data, fieldName):
    if fieldName == 'BTO':
        bto = None
        lyto = response_data['financialDetails']['latestYearTurnover']
        credit = response_data['financialDetails']['annualizedCredit']
        if lyto is not None and credit is not None and credit != 0:
            bto = (lyto/credit) * 100
        return bto
    elif fieldName == 'TOTAL_OBLIGATIONS':
        newValue = None
        totalMonthlyObligations = response_data['obligationSummary']['totalMonthlyObligations']
        if totalMonthlyObligations is not None:
            newValue = totalMonthlyObligations/12
        return newValue
    elif fieldName == 'POS_TURNOVER':
        posAllLoansTurnover = None
        lyto = response_data['financialDetails']['latestYearTurnover']
        posAllLoans = response_data['obligationSummary']['posAllLoans']
        if posAllLoans is not None and lyto is not None and lyto != 0:
            posAllLoansTurnover = posAllLoans/lyto
        return posAllLoansTurnover
    else:
        return None

def excel_reader(request, path, response_data):
    wb = openpyxl.load_workbook(path, data_only=True)
 
    WORKSHEETS_HASHMAP = {}

    for key in c.SCREEN_CELL_MAP:
        response_data[key] = {}
        for eachField in c.SCREEN_CELL_MAP[key]:
            if eachField.cellDetail is not None:
                for cellData in eachField.cellDetail:
                    sheet = None
                    if cellData.sheetName in WORKSHEETS_HASHMAP:
                        sheet = WORKSHEETS_HASHMAP[cellData.sheetName]
                    else:
                        sheet = wb[cellData.sheetName]
                        WORKSHEETS_HASHMAP[cellData.sheetName] = sheet

                    if sheet is None:
                        continue
                    
                    val = sheet[cellData.coordinates].value
                    if isinstance(val, (int, float)):
                        response_data[key][eachField.key] = val
                    else:
                        response_data[key][eachField.key] = None

                    if response_data[key][eachField.key] is not None:
                        break
            if eachField.formula is not None:
                response_data[key][eachField.key] = calculate_fields(response_data, eachField.formula)
    wb.close()
    return response_handler(False,None,response_data)

def fetch_cam_summary_data(body):
    try:
        print("in update_eligibility_excel", body)
        if(body.get('stage')=='UAT'):
            lead_code = uat_lead_code
        else:
            lead_code = body['leadCode']
        
        cognos_to_onedrive = msal.PublicClientApplication(CLIENT_ID, authority=AUTHORITY_URL)
        token = cognos_to_onedrive.acquire_token_by_username_password(USERNAME,PASSWORD,SCOPES)
        
        headers = {'Authorization': 'Bearer {}'.format(token['access_token'])}
        response_get_from_onedrive = get_from_one_drive(file_name  = lead_code, destination = 'sme_underwriting/sme_dashboard_staging/', headers=headers)
        
        if response_get_from_onedrive['error']:
            return response_get_from_onedrive
        
        local_file_path = f'/tmp/{lead_code}.xlsx'
        return excel_reader(request = body, path = local_file_path, response_data = {})
    except Exception as e:
        print(str(e))
        return response_handler(True, str(e), None)