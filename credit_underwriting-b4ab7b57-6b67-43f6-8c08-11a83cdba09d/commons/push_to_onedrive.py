import datetime as dt
import os
import requests
import json
import msal

from commons.config import (CLIENT_ID, AUTHORITY_URL, USERNAME, PASSWORD, SCOPES, RESOURCE_URL, API_VERSION)
from commons.utils import push_to_one_drive, meraki_cam_callback


#Creating a public client app, Aquire a access token for the user and set the header for API calls
cognos_to_onedrive = msal.PublicClientApplication(CLIENT_ID, authority=AUTHORITY_URL)
token = cognos_to_onedrive.acquire_token_by_username_password(USERNAME,PASSWORD,SCOPES)
print(token)
headers = {'Authorization': 'Bearer {}'.format(token['access_token'])}
#onedrive_destination = '{}/{}/me/drive/root:/cognos'.format(RESOURCE_URL,API_VERSION)

def push_to_onedrive(in_file, destination, file_name):
    global headers 
    onedrive_destination = '{}/{}/me/drive/root:/cognos/'.format(RESOURCE_URL,API_VERSION)
    onedrive_destination = onedrive_destination + destination 
    file_data = in_file
    file_data.seek(0,os.SEEK_END)
    print(file_data.tell())
    file_size = file_data.tell()
    file_data.seek(0,0)

    if file_size < 4100000: 
        #Perform is simple upload to the API
        r = requests.put(onedrive_destination+"/"+file_name+":/content", data=file_data, headers=headers)
        file_data.close() 
        return r
    else:
        #Creating an upload session
        upload_session = requests.post(onedrive_destination+"/"+file_name+":/createUploadSession", headers=headers).json()
        
        with open(file_path, 'rb') as f:
            total_file_size = os.path.getsize(file_path)
            chunk_size = 327680
            chunk_number = total_file_size//chunk_size
            chunk_leftover = total_file_size - chunk_size * chunk_number
            i = 0
            while True:
                chunk_data = f.read(chunk_size)
                start_index = i*chunk_size
                end_index = start_index + chunk_size
                # If end of file, break
                if not chunk_data:
                    break
                if i == chunk_number:
                    end_index = start_index + chunk_leftover
                # Setting the header with the appropriate chunk data location in the file
                headers = {'Content-Length':'{}'.format(chunk_size),'Content-Range':'bytes {}-{}/{}'.format(start_index, end_index-1, total_file_size)}
                # Upload one chunk at a time
                chunk_data_upload = requests.put(upload_session['uploadUrl'], data=chunk_data, headers=headers)
                print(chunk_data_upload)
                print(chunk_data_upload.json())
                i = i + 1
            file_data.close() 
        return chunk_data_upload
                
    file_data.close()      
# Looping through the files inside the source directory


def push_files_to_onedrive(file_name, body):
    # file_size = os.stat(file_path).st_size
    todays_date = str(dt.datetime.today().date())
    print(todays_date)
    
    lead_code = file_name.split('/')[-1]
    lead_code = lead_code.split('.xlsx')[0]
    
    print(file_name, lead_code)
    
    if body is not None and body.get('source') == 'MERAKI':
        cognos_to_onedrive = msal.PublicClientApplication(CLIENT_ID, authority=AUTHORITY_URL)
        token = cognos_to_onedrive.acquire_token_by_username_password(USERNAME,PASSWORD,SCOPES)
        
        headers = {'Authorization': 'Bearer {}'.format(token['access_token'])}
        
        local_file_path = f'/tmp/{lead_code}.xlsx'
        response_push_to_onedrive = push_to_one_drive(file_name = lead_code, destination = 'sme_underwriting/sme_dashboard_staging/', headers = headers, path = local_file_path)
        
        if response_push_to_onedrive['error']:
            meraki_cam_callback(body, cam_url= None, error = True, message = response_push_to_onedrive['errorMsg'])
        else:
            meraki_cam_callback(body, cam_url=response_push_to_onedrive['data']['webUrl'], error=False, message = 'cam url generated successfully')
        
    else:
        file_data = open(file_name, 'rb')
        res = push_to_onedrive(file_data, f'sme_underwriting/sme_dashboard_staging/', file_name.split('/')[-1])
        
        if not res.status_code in (200, 201):
            print(f"upload failed with {res.status_code} & returned {res.json()}")
            return
        
        ## call the api to save file link
        res = res.json() if res else  {}
        if res:
            cam_url = res['webUrl']
            url = 'https://seven.prod.growth-source.com/seven/api/update_cam_url/'
            payload = {
                "lead_code" : lead_code,
                "cam_url" : cam_url
            }
            headers = {
                'api-key' : 'e57a015f623c4dd6beaf0b9f13a4b3f8',
                'api-token': '0705f3c880ba4eea8342a12582ef5e1a'
            }
            
            r = requests.post(url, json=payload, headers=headers)
            if not r.status_code == 200:
                print(f"update cam_url failed with status code {r.status_code}")
                print(r.json())
                return 
    
            print(f'pushed_to_onedrive with response {r.json()}')
    