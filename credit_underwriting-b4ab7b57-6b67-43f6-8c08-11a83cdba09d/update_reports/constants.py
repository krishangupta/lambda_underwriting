import update_reports.enums as e
import update_reports.utilities as u

WRITE_FIELDS = ['loanAmount','roi','tenure']

READ_FIELDS = [
    {'name':'currentYearFoir', 'type': 'percentage'},
    {'name':'nextYearFoir', 'type': 'percentage'},
    {'name':'finalAbb', 'type': 'numeric'},
    {'name':'abbEmiRatio', 'type': 'numeric'},
    {'name':'loanAmount', 'type': 'numeric'},
    {'name':'roi', 'type': 'percentage'},
    {'name':'tenure', 'type': 'numeric'}
]

PRODUCT_PROGRAM_MAP = {
    e.Products.SME_BL.value: [
        e.ExcelSheetName.GTP.value, e.ExcelSheetName.GST.value
    ],
    e.Products.SME_LAP.value: [
        e.ExcelSheetName.GTP.value, e.ExcelSheetName.ASSESSED_LAP.value, e.ExcelSheetName.BANKING_SURROGATE.value
    ],
    e.Products.EIL.value: [
        e.ExcelSheetName.GTP.value, e.ExcelSheetName.ASSESSED_EIL.value
    ],
    e.Products.ML.value: [
        e.ExcelSheetName.GTP.value, e.ExcelSheetName.VIABILITY.value
    ]
}

PROGRAM_EXCEL_CELL_MAP = {
    e.EligibilitySubPrograms.ASSESSED_INCOME_SME_LAP.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 23, "column": 'D'}),
        "roi": u.ExcelAxis(**{"row": 22, "column": 'D'}),
        "tenure": u.ExcelAxis(**{"row": 21, "column": 'D'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 18, "column": 'C'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 18, "column": 'D'}),
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.ASSESSED_INCOME_EIL.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 25, "column": 'E'}),
        "roi": u.ExcelAxis(**{"row": 24, "column": 'E'}),
        "tenure": u.ExcelAxis(**{"row": 23, "column": 'E'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 20, "column": 'D'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 20, "column": 'E'}),
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.GTP_SME_BL.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 30, "column": 'G'}),
        "roi": u.ExcelAxis(**{"row": 29, "column": 'G'}),
        "tenure": u.ExcelAxis(**{"row": 28, "column": 'G'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 24, "column": 'I'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 24, "column": 'J'}),
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.GTP_SME_LAP.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 30, "column": 'G'}),
        "roi": u.ExcelAxis(**{"row": 29, "column": 'G'}),
        "tenure": u.ExcelAxis(**{"row": 28, "column": 'G'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 24, "column": 'I'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 24, "column": 'J'}),
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.GTP_EIL.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 30, "column": 'G'}),
        "roi": u.ExcelAxis(**{"row": 29, "column": 'G'}),
        "tenure": u.ExcelAxis(**{"row": 28, "column": 'G'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 24, "column": 'I'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 24, "column": 'J'}),
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.GTP_ML.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 30, "column": 'G'}),
        "roi": u.ExcelAxis(**{"row": 29, "column": 'G'}),
        "tenure": u.ExcelAxis(**{"row": 28, "column": 'G'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 24, "column": 'I'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 24, "column": 'J'}),
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.VIABILITY_ML.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 20, "column": 'C'}),
        "roi": u.ExcelAxis(**{"row": 19, "column": 'C'}),
        "tenure": u.ExcelAxis(**{"row": 18, "column": 'C'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 16, "column": 'C'}),
        "nextYearFoir": None,
        "finalAbb": None,
        "abbEmiRatio": None
    }),
    e.EligibilitySubPrograms.BANKING_SURROGATE_SME_LAP.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 13, "column": 'F'}),
        "roi": u.ExcelAxis(**{"row": 15, "column": 'F'}),
        "tenure": u.ExcelAxis(**{"row": 14, "column": 'F'}),
        "currentYearFoir": None,
        "nextYearFoir": None,
        "finalAbb": u.ExcelAxis(**{"row": 3, "column": 'H'}),
        "abbEmiRatio": u.ExcelAxis(**{"row": 3, "column": 'I'}),
    }),
    e.EligibilitySubPrograms.GST_SME_BL.value: u.EligibilityProgramFields(**{
        "loanAmount": u.ExcelAxis(**{"row": 17, "column": 'F'}),
        "roi": u.ExcelAxis(**{"row": 16, "column": 'F'}),
        "tenure": u.ExcelAxis(**{"row": 15, "column": 'F'}),
        "currentYearFoir": u.ExcelAxis(**{"row": 11, "column": 'H'}),
        "nextYearFoir": u.ExcelAxis(**{"row": 11, "column": 'I'}),
        "finalAbb": None,
        "abbEmiRatio": None,
    })
}