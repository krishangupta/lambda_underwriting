from commons.common_functions import QueryAthena, HelperClassS3, copy_cell_value_openpyxl, copy_df_to_excel_with_formatting, copy_df_to_excel_with_formatting_rtr
from commons.push_to_onedrive import push_files_to_onedrive
from commons.formulas_writing import case_details_formulas, fs_formulas, revenue_estimation_formulas, gtp_lap_bl_formulas, rtr_formulas
from commons.populate_sheets import query_handler, populate_status_sheet, populate_additional_FS_data_points, populate_customer_details, rtr_sheet, gsft_score_and_bank_score_df_fun, gtp_lap_bl, set_gtp_values, revenue_estimation, set_revenue_values, validate_revenue_estimation_output, school_data 
from dateutil.relativedelta import relativedelta

import commons.custom_exceptions as custom_exceptions
import sub_reports.data_config as data_config
import commons.db_queries as db_query
import datetime as dt
import pandas as pd
import numpy as np
import os
import boto3
import openpyxl
import json
import copy

def generate_report(loan_id,cbse_aff_id=None):
    try:
        
        if cbse_aff_id:
            school_df = query_handler(db_query.cbse_school_data.replace('<cbse_aff_id>', cbse_aff_id), database='default', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
            if len(school_df)==0:
                local_template_path = '/tmp/revenue_estimation_template_v12.xlsx'
                eil = False
            else:
                local_template_path = '/tmp/revenue_estimation_template_eil_v5.xlsx'
                school_sr = school_df.iloc[0,]
                eil = True
                print("using eil template")
        else :
            local_template_path = '/tmp/revenue_estimation_template_v12.xlsx'
            eil = False
        
        rtr_df = query_handler(db_query.loan_level_rtr_query.replace('<loan_no>', loan_id), database='default', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        rtr_df.replace(np.nan, '', regex=True, inplace=True)
        rtr_df.sort_values(by=['account_status' , 'disbursed_amt'] , inplace=True , ascending=True)
        ### change data type from datetime to date -- rtr_df
        rtr_df['disbursed_dt'] = pd.to_datetime(rtr_df['disbursed_dt']).dt.date
        rtr_df['date_reported'] = pd.to_datetime(rtr_df['date_reported']).dt.date

        comm_rtr_df = query_handler(db_query.loan_level_comm_rtr_query.replace('<loan_no>', loan_id), database = 'tp_source_db', filter_clause='loan_number', filter_variable=loan_id, filter_required=False)
        comm_rtr_df.replace(np.nan, '', regex=True, inplace=True)
        ### change data type from datetime to date
        comm_rtr_df['sanction_date'] = pd.to_datetime(comm_rtr_df['sanction_date']).dt.date
        comm_rtr_df['last_reported_date'] = pd.to_datetime(comm_rtr_df['last_reported_date']).dt.date
        
        gst_month_df = query_handler(db_query.loan_level_gst_month_query, database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id)
   
        gst_state_df = query_handler(db_query.loan_level_gst_state_query, database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id)
        gst_state_df.replace(np.nan, '', regex=True, inplace=True)

        
        gsft_score_df = query_handler(db_query.loan_level_gsft_score_query, database='default', filter_clause='lead_code', filter_variable=loan_id)
        gsft_score_df.replace(np.nan, '', regex=True, inplace=True)

        #get bank score df
        bank_score_df = query_handler(db_query.loan_level_bank_score_query.replace('<loan_no>', loan_id), database='gsft-analytics-db', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        bank_score_df.replace(np.nan, '', regex=True, inplace=True)
        
        bre_df = query_handler(db_query.loan_level_bre_query, database='los-afx-db', filter_clause='loan_number', filter_variable=loan_id)
        bre_df.replace(np.nan, '', regex=True, inplace=True)
        
                
        fs_score_df = query_handler(db_query.loan_level_fs_data_query, database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id)
        fs_score_df.replace(np.nan, '', regex=True, inplace=True)
        fs_score_df.sort_values(by=['fy'], inplace=True, ascending=True)

        # fs Transpose
        fs_score_df = fs_score_df.T
        
        fs_score_df_fix = fs_score_df.rename(columns=fs_score_df.iloc[0]) 
        
        fs_score_df_columns = []
        for i in range(0, len(fs_score_df.columns)):
            fs_score_df_columns.append(fs_score_df[i]['fy'])
    
        fs_score_df.columns = fs_score_df_columns
        fs_score_df.drop(['fy'], inplace=True)
        fs_score_df.reset_index(inplace=True)
        fs_score_df.rename(columns={'index':''}, inplace=True)
        
        banking_df = query_handler(db_query.loan_level_banking_query.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        
        pos_df = query_handler(db_query.loan_level_pos_query.replace('<loan_no>', loan_id), database='tp_source_db', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        pos_df.replace(np.nan, '', regex=True, inplace=True)
        
        
        credit_queue_df = query_handler(db_query.credit_queue_date.replace('<loan_no>', loan_id), database = 'afx_master', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        address_df = query_handler(db_query.address_details.replace('<loan_no>', loan_id), database = 'afx_master', filter_clause='lead_code', filter_variable=loan_id, filter_required=False)
        
            
        
        
        # populate workbook
        workbook = openpyxl.load_workbook(local_template_path)
        
        excel_writer = pd.ExcelWriter(local_template_path, engine='openpyxl')
        excel_writer.book = workbook
        excel_writer.sheets = dict((ws.title, ws) for ws in workbook.worksheets)
        
        excel_writer.sheets['GST'] = workbook.create_sheet('GST')
        excel_writer.sheets['gsft_score'] = workbook.create_sheet('gsft_score')
        excel_writer.sheets['FS1'] = workbook.create_sheet('FS1')
        excel_writer.sheets['RTR_MASTER'] = workbook.create_sheet('RTR_MASTER')
        excel_writer.sheets['banking_summary'] = workbook.create_sheet('banking_summary')
        excel_writer.sheets['BRE'] = workbook.create_sheet('BRE')
        excel_writer.sheets['COMM_RTR_MASTER'] = workbook.create_sheet('COMM_RTR_MASTER')
        excel_writer.sheets['HISTORIC_LIABILITY'] = workbook.create_sheet('HISTORIC_LIABILITY')
        
        
       
        
        
        copy_df_to_excel_with_formatting(rtr_df, excel_writer.sheets['RTR_MASTER'], start_row=0) # rtr_df.to_excel(excel_writer, sheet_name='RTR_MASTER', index=None)
        copy_df_to_excel_with_formatting(gst_month_df, excel_writer.sheets['GST'], start_row=0) # gst_month_df.to_excel(excel_writer, sheet_name='GST', startrow=0, startcol=0, index=None)
        copy_df_to_excel_with_formatting(gst_state_df, excel_writer.sheets['GST'], start_row=0, start_column=len(list(gst_month_df))+2) # gst_state_df.to_excel(excel_writer, sheet_name='GST', startrow=len(gst_month_df)+2, startcol=0, index=None)
        #copy_df_to_excel_with_formatting(gsft_score_df, excel_writer.sheets['gsft_score'], start_row=0) # gsft_score_df.to_excel(excel_writer, sheet_name='gsft_score', engine='openpyxl', index=None)
        copy_df_to_excel_with_formatting(fs_score_df_fix, excel_writer.sheets['FS1'], start_row=0) # fs_score_df.to_excel(excel_writer, sheet_name='FS', engine='openpyxl', index=None)
        copy_df_to_excel_with_formatting(banking_df, excel_writer.sheets['banking_summary'], start_row=0)
        copy_df_to_excel_with_formatting(bre_df, excel_writer.sheets['BRE'], start_row=0)
        copy_df_to_excel_with_formatting(comm_rtr_df, excel_writer.sheets['COMM_RTR_MASTER'], start_row=0)
        copy_df_to_excel_with_formatting(pos_df, excel_writer.sheets['HISTORIC_LIABILITY'], start_row=0)
    
        print(excel_writer.book.sheetnames)
        
        revenue_estimation(gst_month_df, fs_score_df_fix, excel_writer.sheets['revenue_estimation'])
        gtp_lap_bl(fs_score_df_fix, excel_writer.sheets['GTP_LAP_BL'])
        rtr_template_df = rtr_sheet(rtr_df,comm_rtr_df, excel_writer.sheets['RTR'])
        copy_df_to_excel_with_formatting(rtr_template_df, excel_writer.sheets['RTR'], start_row=18, auto_filter_row='RTR') # rtr_template_df.to_excel(excel_writer, sheet_name='RTR', startrow=18, startcol=0, index=None)
        #rtr_total_row(rtr_template_df, excel_writer.sheets['RTR'])
        populate_additional_FS_data_points(excel_writer.sheets['FS'], loan_id)
        populate_customer_details(excel_writer.sheets['SUMMARY'], loan_id)
        
        populate_status_sheet(credit_queue_df,address_df, excel_writer.sheets['Status'])
        
        
        
        #concatenate gsft score df & bank score df into one df, then to have both on GSFT SCORE sheet
        gsft_and_bank_score_df = gsft_score_and_bank_score_df_fun(gsft_score_df, bank_score_df)
        copy_df_to_excel_with_formatting(gsft_and_bank_score_df, excel_writer.sheets['gsft_score'], start_row=0)
        
        #case_details_formulas(excel_writer.sheets['case_details'])
        fs_formulas(excel_writer.sheets['FS'])
        rtr_formulas(excel_writer.sheets['RTR'])
        revenue_estimation_formulas(excel_writer.sheets['revenue_estimation'])
        gtp_lap_bl_formulas(excel_writer.sheets['GTP_LAP_BL'])
        
        if eil:
            print("writing school data")
            school_data(excel_writer.sheets['Student Data and collections'], school_sr)

        excel_writer.save()
        return {
            'local_file_name':local_template_path,
            'gst_month_df':len(gst_month_df),
            'gst_state_df':len(gst_state_df),
            'gsft_score_df':len(gsft_score_df),
            'fs_score_df':len(fs_score_df)
        }


    except custom_exceptions.rtr_DNF as ex:
        return {"status":"fail", "df":"rtr_df", "len":0, "table":"tp_source_db.cons_cibil_tradelines", "loan_id":f"{loan_id}"}
    except custom_exceptions.gst_month_DNF as ex:
        return {"status":"fail", "df":"gst_month_df", "len":0, "table":"tp_source_db.gst_sales_and_purchase_month", "loan_id":f"{loan_id}"}
    except custom_exceptions.gst_state_DNF as ex:
        return {"status":"fail", "df":"gst_state_df", "len":0, "table":"tp_source_db.gst_sales_and_purchase_state", "loan_id":f"{loan_id}"}
    except custom_exceptions.gsft_score_DNF as ex:
        return {"status":"fail", "df":"gsft_score_df", "len":0, "table":"gsft-analytics-db.cons_cibil_gsft_score_detail", "loan_id":f"{loan_id}"}
    except custom_exceptions.fs_score_DNF as ex:
        return {"status":"fail", "df":"fs_score_df", "len":0, "table":"tp_source_db.financial_statement_data", "loan_id":f"{loan_id}"}
    #except custom_exceptions.banking_DNF as ex:
    #    return {"status":"fail", "df":"banking_df", "len":0, "table":"tp_source_db.bs_account_transactions", "loan_id":f"{loan_id}"}



    
def run_report(loan_id,cbse_aff_id=None, body=None):
    bucket_name = 'datalake-gsft'
    s3_helper = HelperClassS3()
    
    s3_helper.copy_s3_file_to_local('/tmp/', 'revenue_estimation_template_v12.xlsx', bucket_name, 'sme_underwriting/')
    s3_helper.copy_s3_file_to_local('/tmp/', 'revenue_estimation_template_eil_v5.xlsx', bucket_name, 'sme_underwriting/')

    return_json = generate_report(loan_id,cbse_aff_id)
    print("generate_report called")
    
    if return_json.get('status', None)=='fail':
        return return_json
    
    local_file_name = return_json['local_file_name']
    local_file_path = '/'.join(local_file_name.split('/')[:-1])+'/'
    local_file_name = local_file_name.split('/')[-1]
    s3_file_name = f'{loan_id}.xlsx'
    
    os.rename(local_file_path+local_file_name, local_file_path+s3_file_name)
    print("os rename called")
    return_json['local_file_name'] = local_file_path+s3_file_name
    todays_date = str(dt.datetime.today().date())
    
    print("s3 push init")
    return_json['s3_file_name'] = 's3://' + bucket_name + '/' + s3_helper.copy_local_file_to_s3(local_file_path, s3_file_name, bucket_name, f'sme_underwriting/reports/{todays_date}/')
    print("s3 push completed")
    return_json.update(validate_revenue_estimation_output(return_json))
    print("validate_revenue_estimation_output json updated")
    push_files_to_onedrive(return_json['local_file_name'], body)
    
    return return_json
    
