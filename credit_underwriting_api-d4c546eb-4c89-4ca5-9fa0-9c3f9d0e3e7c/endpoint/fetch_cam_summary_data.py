import json
import boto3
import time
import botocore

#LOCAL IMPORTS
from common_functions.constants import function_arn

client = boto3.client('lambda')

def fetch_cam_summary_data(body):
    print("in fetch_cam_summary_data")
    
    response = client.invoke(
        FunctionName = function_arn,
        InvocationType = 'RequestResponse',
        Payload = json.dumps(body)
    )
 
    responseFromChild = json.load(response['Payload'])
 
    print('\n')
    print(responseFromChild)
    
    
    return responseFromChild